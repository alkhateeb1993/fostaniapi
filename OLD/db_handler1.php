<?php

class DbHandler {

    public $conn;
    public $sub_hours = '1';

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        //opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    //create new user if not exist
    public function createUser($user_name, $user_password, $user_email, $user_mobile, $user_country_id, $user_gender, $user_regid, $user_pic) {

        $response = array();

        //Check if the user is not exist in DB
        if (!$this->isEmailExist($user_email)) {
            if (!$this->isUsernameExists($user_name)) {
                if (!empty($user_pic)) {
                    $filePath = $this->DecodeImage($user_pic);
                    if ($filePath != null) {

                        $stmt = $this->conn->prepare(
                                "INSERT INTO users (user_name, user_password,user_email,user_mobile,user_image,user_country_id,"
                                . "user_gender,gcm_registration_id) VALUES(?,?,?,?,?,?,?,?)");
                        $stmt->bind_param("sssssiss", $user_name, $user_password, $user_email, $user_mobile, $filePath, $user_country_id, $user_gender, $user_regid);

                        $result = $stmt->execute();
                        $stmt->close();

                        if ($result) {
                            //User Successfully inserted
                            $response["error"] = false;
                            $response['message_en'] = "Registerd Successfully";
                            $response["message_ar"] = "تم التسجيل بنجاح";
                            $response['user'] = array();
                            array_push($response['user'], $this->getUserByEmail($user_email));
                        } else {
                            //Failed to create user
                            $response["error"] = true;
                            $response['message_en'] = "An error occured while registering";
                            $response["message_ar"] = "حصلت مشكله اثناء التسجيل";
                        }
                    } else {
                        $response["error"] = true;
                        $response['message_en'] = "Error While Uploading The Image";
                        $response["message_ar"] = "حصلت مشكله اثناء رفع الصوره";
                    }
                } else {
                    $stmt = $this->conn->prepare(
                            "INSERT INTO users (user_name, user_password,user_email,user_mobile,user_country_id,"
                            . "user_gender,gcm_registration_id) VALUES(?,?,?,?,?,?,?,?)");
                    $stmt->bind_param("sssssiss", $user_name, $user_password, $user_email, $user_mobile, $user_country_id, $user_gender, $user_regid);

                    $result = $stmt->execute();
                    $stmt->close();

                    if ($result) {
                        //User Successfully inserted
                        $response["error"] = false;
                        $response['message_en'] = "Registerd Successfully";
                        $response["message_ar"] = "تم التسجيل بنجاح";
                        $response['user'] = array();
                        array_push($response['user'], $this->getUserByEmail($user_email));
                    } else {
                        //Failed to create user
                        $response["error"] = true;
                        $response['message_en'] = "An error occured while registering";
                        $response["message_ar"] = "حصلت مشكله اثناء التسجيل";
                    }
                }
            } else {
                $response["error"] = true;
                $response['message_en'] = "This username is already registerd";
                $response["message_ar"] = "اسم المستخدم موجود مسبقا";
            }
        } else {
            $response["error"] = true;
            $response['message_en'] = "User with same email already exists";
            $response["message_ar"] = "البريد الالكتروني معرف مسبقا";
        }
        return $response;
    }

    //Login
    public function login($user_email, $user_password) {
        $response = array();

        $stmt = $this->conn->prepare("select * from users where user_email = ? and user_password = ?");
        $stmt->bind_param("ss", $user_email, $user_password);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        if ($num_rows > 0) {
            $stmt->bind_result($user_id, $user_name, $user_email, $user_password, $user_mobile, $user_image, $user_country_id, $user_gender, $user_created_at, $user_active, $gcm_registration_id);
            $stmt->fetch();
            $userData = array();

            $userData["user_id"] = $user_id;
            $userData["user_name"] = $user_name;
            $userData["user_password"] = $user_password;
            $userData["user_email"] = $user_email;
            $userData["user_mobile"] = $user_mobile;
            $userData["user_image"] = $user_image;
            $userData["user_country_id"] = $user_country_id;
            $userData["user_gender"] = $user_gender;
            $userData["user_created_at"] = $user_created_at;
            $userData["user_active"] = $user_active;
            $userData["user_regid"] = $gcm_registration_id;


            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $userData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "Username Or Password Is Incorrect";
            $response["message_ar"] = "اسم المستخدم او كلمة المرور غير صحيح";

            return $response;
        }
    }

    // fetching single user by id
    public function getUser($user_id) {
        $stmt = $this->conn->prepare("SELECT user_id, user_name, user_email, gcm_registration_id, user_created_at FROM users WHERE user_id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($USER_ID, $USER_NAME, $USER_EMAIL, $registration_id, $USER_CREATED_AT);
            $stmt->fetch();
            $user = array();
            $user["USER_ID"] = $USER_ID;
            $user["USER_NAME"] = $USER_NAME;
            $user["USER_EMAIL"] = $USER_EMAIL;
            $user["registration_id"] = $registration_id;
            $user["USER_CREATED_AT"] = $USER_CREATED_AT;

            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    //UPDATE USER INFO
    public function updateUserInfo($user_id, $user_name, $user_password, $user_mobile, $user_country_id) {
        $response = array();

        if (!empty($user_pic)) {
            $filePath = $this->DecodeImage($user_pic);
            if ($filePath != null) {
                $stmt = $this->conn->prepare("UPDATE users SET user_name = ? , user_country_id = ? , user_password  = ? "
                        . ", user_image = ? , user_mobile  = ?  "
                        . " WHERE user_id = ? ");
                $stmt->bind_param("sisssi", $user_name, $user_country_id, $user_password, $filePath, $user_mobile, $user_id);
                $stmt->execute();
                $stmt->store_result();
                $num_rows = $stmt->affected_rows;
                if ($num_rows > 0) {
                    // User successfully updated
                    $response["error"] = false;
                    $response['message_en'] = 'Account Info has been updated';
                    $response["message_ar"] = "تم تعديل الملف الشخصي";
                    $response['user'] = array();
                    array_push($response['user'], $this->getUserByEmail($user_email));

                    $this->commitDB();
                } else {
                    // Failed to update user
                    $response["error"] = true;
                    $response['message_en'] = "Failed to update account info";
                    $response["message_ar"] = "لا يمكن تعديل الملف الشخصي";
                    $stmt->error;
                }
            } else {
                $response["error"] = true;
                $response['message_en'] = "Error While Uploading The Image";
                $response["message_ar"] = "حصلت مشكله اثناء رفع الصورة";
            }
        } else {
            $stmt = $this->conn->prepare("UPDATE users SET user_name = ? , user_country_id = ? , user_password  = ? "
                    . ", user_mobile  = ?  "
                    . " WHERE user_id = ? ");
            $stmt->bind_param("sissi", $user_name, $user_country_id, $user_password, $user_mobile, $user_id);
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->affected_rows;
            if ($num_rows > 0) {
                // User successfully updated
                $response["error"] = false;
                $response['message_en'] = 'Account Info has been updated successfully';
                $response['user'] = array();
                array_push($response['user'], $this->getUserByEmail($user_email));

                $this->commitDB();
            } else {
                // Failed to update user
                $response["error"] = true;
                $response['message_en'] = "Failed to update account info";
                $response["message_ar"] = "لا يمكن تعديل الملف الشخصي";
                $stmt->error;
            }
        }


        $stmt->close();

        return $response;
    }

    public function updateRegID($user_id, $reg_id) {
        $response = array();
        $stmt = $this->conn->prepare("UPDATE users SET gcm_registration_id = ? "
                . " WHERE user_id = ? ");
        $stmt->bind_param("si", $reg_id, $user_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        $stmt->close();

        if ($num_rows > 0) {
            $this->commitDB();
            return true;
        } else {
            // Failed to update user
            return false;
        }
    }

    /*
        * Fostans Table *****************************
     */

    //create Fostan
    public function createFostan($fostanSize, $fostanType, $fostanFor, $fostanPrice, $fostanTitle, $fostanDescription, $user_id, $show_number, $fostan_city, $fostanImg1, $fostanImg2, $fostanImg3) {

        $PhotoPath1 = $this->DecodeImage($fostanImg1);
        $PhotoPath2 = $this->DecodeImage($fostanImg2);
        $PhotoPath3 = $this->DecodeImage($fostanImg3);

        $stmt = $this->conn->prepare(
                "insert into fostans( fostan_title,fostan_proccess, fostan_price,fostan_size,fostan_desc,fostan_img1,fostan_img2,fostan_img3,fostan_city,show_number,user_id,fostan_type_id)"
                . " VALUES (?,?,?,?)");
        $stmt->bind_param("ssssssssiiii", $fostanTitle, $fostanFor, $fostanPrice, $fostanSize, $fostanDescription, $PhotoPath1, $PhotoPath2, $PhotoPath3, $fostan_city, $show_number, $user_id, $fostanType);

        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            return $this->conn->insert_id;
        } else {
            return null;
        }

        return null;
    }

    //create Fostans Attachment Assosiciation
    public function createFostansAttachments($fostan_id, $attachment_id) {
        $stmt = $this->conn->prepare(
                "insert into fostans_attachments "
                . " VALUES (?,?)");
        $stmt->bind_param("ii", $attachment_id, $fostan_id);
        $result = $stmt->execute();
        $stmt->close();
    }

    //DELETE FOSTAN
    public function deleteFostan($FOSTAN_ID) {
        $response = array();
        $stmt = $this->conn->prepare("DELETE FROM fostans WHERE FOSTAN_ID = ? ");
        $stmt->bind_param("i", $FOSTAN_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            // User successfully updated
            $response["error"] = false;
            $response['message_en'] = 'Fostan Has Been Deleted Successfully';

            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to delete the fostan";
        }

        return $response;
    }

    //UPDATE FOSTAN
    public function updateFostan($FOSTAN_ID, $FOSTAN_NAME, $FOSTAN_DESC) {
        $response = array();

        if (!empty($FOSTAN_DESC)) {
            $stmt = $this->conn->prepare("UPDATE fostans SET FOSTAN_NAME = ? , FOSTAN_DESC  = ?  , FOSTAN_UPDATED_AT = now()"
                    . " WHERE FOSTAN_ID = ? ");
            $stmt->bind_param("ssi", $FOSTAN_NAME, $FOSTAN_DESC, $FOSTAN_ID);
        } else {
            $stmt = $this->conn->prepare("UPDATE fostans SET FOSTAN_NAME = ?  , FOSTAN_UPDATED_AT = now()"
                    . " WHERE FOSTAN_ID = ? ");
            $stmt->bind_param("si", $FOSTAN_NAME, $FOSTAN_ID);
        }

        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows) {
            return $num_rows;
        } else {
            return null;
        }
        $stmt->close();


        return $response;
    }

    //Get Fostan By ID
    public function getFostanByID($fostan_id) {
        $stmt = $this->conn->prepare("SELECT c.* , u.user_full_name FROM fostans c , users u where 
                                        c.FOSTAN_ID = ? and u.user_id = c.instructor_id ;");
        $stmt->bind_param("i", $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($FOSTAN_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $DEPT_ID, $FOSTAN_DESC, $FOSTAN_CREATED_AT, $FOSTAN_UPDATED_AT, $user_full_name);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['INSTRUCTOR_ID'] = $INSTRUCTOR_ID;
                $temp['INSTRUCTOR_NAME'] = $user_full_name;
                $temp['DEPT_ID'] = $DEPT_ID;
                $temp['FOSTAN_DESC'] = $FOSTAN_DESC;
                $temp['FOSTAN_CREATED_AT'] = $FOSTAN_CREATED_AT;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "Error while getting the fostan";
        }
        $stmt->close();
        return $response;
    }

    //Get All fostans assigned to the department
    public function getAllFostansDept($start, $end, $dept_id) {
        $stmt = $this->conn->prepare("SELECT * , u.user_full_name FROM fostans c , users u where "
                . "DEPT_ID = ? and u.user_id = c.instructor_id order by fostan_id desc LIMIT ?,?");
        $stmt->bind_param("iii", $dept_id, $start, $end);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($FOSTAN_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $DEPT_ID, $FOSTAN_DESC, $FOSTAN_CREATED_AT, $FOSTAN_UPDATED_AT, $USER_ID, $user_full_name, $user_name, $user_email, $USER_PASSWORD, $user_pic, $USER_GENDER, $user_mobile, $user_rule, $user_dept, $USER_CREATED_AT, $registration_id, $user_full_name);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['INSTRUCTOR_ID'] = $INSTRUCTOR_ID;
                $temp['INSTRUCTOR_NAME'] = $user_full_name;
                $temp['DEPT_ID'] = $DEPT_ID;
                $temp['FOSTAN_DESC'] = $FOSTAN_DESC;
                $temp['FOSTAN_CREATED_AT'] = $FOSTAN_CREATED_AT;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "Error while getting all fostans for the department";
        }
        $stmt->close();
        return $response;
    }

    //Get All fostans assigned to the department
    public function getAllUsersForFostan($FOSTAN_ID) {
        $stmt = $this->conn->prepare("SELECT
                                            U.USER_ID,
                                            U.USER_FULL_NAME
                                          FROM
                                            users U,
                                            fostans_student CS
                                          WHERE
                                            CS.fostan_id = ? AND CS.student_id = U.USER_ID");
        $stmt->bind_param("i", $FOSTAN_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($USER_ID, $USER_FULL_NAME);

        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            //
            while ($stmt->fetch()) {
                $temp = array();
                $temp['USER_ID'] = $USER_ID;
                $temp['USER_FULL_NAME'] = $USER_FULL_NAME;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Students Found Joined For This Fostan";
        }
        $stmt->close();
        return $response;
    }

    //Check if the fostan has records in student_marks
    public function fostanGradesSubmitted($FOSTAN_ID) {
        $stmt = $this->conn->prepare("SELECT * FROM student_marks WHERE FOSTAN_ID = ?");
        $stmt->bind_param("i", $FOSTAN_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        if ($num_of_rows > 0) {
            return true;
        }
        return false;
    }

    //Get -** All **- fostans to the Student joined and not joined fostans with column named isJoined
    public function getAllFostansStudent($start, $end, $student_id, $dept_id) {
        $stmt = $this->conn->prepare("SELECT c.*, u.USER_FULL_NAME,
                                            (
                                              CASE WHEN(
                                              SELECT
                                                COUNT(*)
                                              FROM
                                                fostans_student cs
                                              WHERE
                                                cs.fostan_id = c.FOSTAN_ID AND cs.student_id = ?
                                            ) > 0 THEN TRUE ELSE FALSE
                                            END
                                          ) AS isJoined
                                          FROM
                                            fostans c,
                                            users u
                                          WHERE
                                            c.DEPT_ID = ? AND c.INSTRUCTOR_ID = u.USER_ID
                                          ORDER BY
                                            c.FOSTAN_ID
                                          DESC
                                          LIMIT ?,?");
        $stmt->bind_param("iiii", $student_id, $dept_id, $start, $end);
        $stmt->execute();

        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($FOSTAN_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $DEPT_ID, $FOSTAN_DESC, $FOSTAN_CREATED_AT, $FOSTAN_UPDATED_AT, $USER_FULL_NAME, $isJoined);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['DEPT_ID'] = $DEPT_ID;
                $temp['FOSTAN_DESC'] = $FOSTAN_DESC;
                $temp['INSTRUCTOR_ID'] = $INSTRUCTOR_ID;
                $temp['INSTRUCTOR_NAME'] = $USER_FULL_NAME;
                $temp['FOSTAN_CREATED_AT'] = $FOSTAN_CREATED_AT;
                $temp['isJoined'] = $isJoined;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
        }
        $stmt->close();
        return $response;
    }

    //Get fostans to the Student only joined fostans
//    public function getFostansStudent($start, $end, $student_id) {
//        $stmt = $this->conn->prepare("SELECT c.* , u.USER_FULL_NAME from fostans c , users u , fostans_student cs
//                                            WHERE
//                                            cs.fostan_id = c.FOSTAN_ID AND
//                                            cs.student_id = u.USER_ID AND
//                                            cs.student_id = ?
//                                            LIMIT ?,?");
//        $stmt->bind_param("iii", $student_id, $start, $end);
//        $stmt->execute();
//
//        $fostanss = $stmt->get_result();
//        $num_rows = $fostanss->num_rows;
//
//        if ($num_rows > 0) {
//            $stmt->close();
//            return $fostanss;
//        } else {
//            $stmt->close();
//            return null;
//        }
//    }
    //Get All fostans assigned to the instructor
    public function getAllFostansInstructor($start, $end, $instructor_id) {
        $stmt = $this->conn->prepare("select c.FOSTAN_ID,c.FOSTAN_NAME,c.INSTRUCTOR_ID,c.DEPT_ID,c.FOSTAN_DESC ,c.FOSTAN_CREATED_AT , u.USER_FULL_NAME  "
                . "from  fostans c , users u "
                . "where c.INSTRUCTOR_ID = ? and "
                . "c.INSTRUCTOR_ID = u.user_id"
                . " ORDER BY c.FOSTAN_ID DESC LIMIT ?,?");
        $stmt->bind_param("iii", $instructor_id, $start, $end);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($FOSTAN_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $DEPT_ID, $FOSTAN_DESC, $FOSTAN_CREATED_AT, $USER_FULL_NAME);

        $response = array();
        if ($num_of_rows > 0) {
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['DEPT_ID'] = $DEPT_ID;
                $temp['INSTRUCTOR_ID'] = $INSTRUCTOR_ID;
                $temp['INSTRUCTOR_NAME'] = $USER_FULL_NAME;
                $temp['FOSTAN_DESC'] = $FOSTAN_DESC;
                $temp['FOSTAN_CREATED_AT'] = $FOSTAN_CREATED_AT;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found";
        }

        $stmt->close();
        return $response;
    }

    //Get Attachments for the fostan
    public function getAttachmentsForFostan($fostan_id) {
        $stmt = $this->conn->prepare("SELECT
                                a.ATTACHMENT_ID,
                                a.ATTACHMENT_NAME,
                                a.ATTACHMENT_URL,
                                ca.cs_id
                              FROM
                                attachments a,
                                fostans c,
                                fostans_attachments ca
                              WHERE
                                c.FOSTAN_ID = ? AND ca.attachment_id = a.ATTACHMENT_ID AND ca.fostan_id = c.FOSTAN_ID ");
        $stmt->bind_param("i", $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($ATTACHMENT_ID, $ATTACHMENT_NAME, $ATTACHMENT_URL, $cs_id);

        $response = array();
        if ($num_of_rows > 0) {
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['ATTACHMENT_ID'] = $ATTACHMENT_ID;
                $temp['ATTACHMENT_NAME'] = $ATTACHMENT_NAME;
                $temp['ATTACHMENT_URL'] = $ATTACHMENT_URL;
                $temp['CS_ID'] = "" . $cs_id;

                array_push($response['data'], $temp);
            }
        } else {

            $response['error'] = true;
            $response['message_en'] = "No Records Found";
        }
        $stmt->close();
        return $response;
    }

    //Update Attachments for the fostan
    public function updateAttachmentForFostan($FOSTAN_ID, $ATTACHMENT_ID, $CS_ID) {
        $response = array();

        $stmt = $this->conn->prepare("UPDATE fostans_attachments SET attachment_id = ? , fostan_id  = ? "
                . " WHERE cs_id = ? ");
        $stmt->bind_param("iii", $ATTACHMENT_ID, $FOSTAN_ID, $CS_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            return $num_rows;
        } else {
            return null;
        }
        $stmt->close();


        return $response;
    }

    //Delete Attachments for the fostan
    public function removeAttachmentForFostan($cs_id) {

        $response = array();
        $stmt = $this->conn->prepare("delete from fostans_attachments where cs_id = ?");
        $stmt->bind_param("i", $cs_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            return $num_rows;
        } else {
            return null;
        }
    }

    //Join Fostan
    public function joinFostan($student_id, $fostan_id) {
        $response = array();

        //insert query
        $stmt = $this->conn->prepare(
                "insert into fostans_student "
                . " values (?,?)");
        $stmt->bind_param("ii", $fostan_id, $student_id);

        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            //User Successfully inserted
            $response["error"] = false;
            $response['message_en'] = "You've been joined successfully";
            $this->commitDB();
        } else {
            $response["error"] = true;
            $response['message_en'] = "An error occured while creating the fostan, Maybe you joined before";
        }
        return $response;
    }

    //Disjoin Fostan
    public function disJoinFostan($FOSTAN_ID, $STUDENT_ID) {
        $response = array();
        $stmt = $this->conn->prepare("DELETE FROM fostans_student WHERE FOSTAN_ID = ? and student_id = ? ");
        $stmt->bind_param("ii", $FOSTAN_ID, $STUDENT_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            // User successfully updated
            $response["error"] = false;
            $response['message_en'] = 'Fostan Has Been Disjoined Successfully';
            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to disjoin with this fostan";
        }

        return $response;
    }

    //Get Joined Fostans
    public function getJoinedfostans($student_id) {
        $stmt = $this->conn->prepare("SELECT cs.FOSTAN_ID , c.FOSTAN_NAME "
                . "FROM fostans_student cs , fostans c "
                . "where c.fostan_id = cs.fostan_id AND "
                . "cs.student_id = ?");
        $stmt->bind_param("i", $student_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($FOSTAN_ID, $FOSTAN_NAME);

        $response = array();
        if ($num_of_rows > 0) {
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;

                array_push($response['data'], $temp);
            }
        } else {

            $response['error'] = true;
            $response['message_en'] = "No Records Found";
        }
        $stmt->close();
        return $response;
    }

    //GET Submited Quizes Fostans
    public function getSubmitedQuizesFostans($student_id) {
        $stmt = $this->conn->prepare("SELECT DISTINCT
                                            C.FOSTAN_ID,
                                            C.FOSTAN_NAME
                                          FROM
                                            quiz Q,
                                            quiz_result QR,
                                            fostans C
                                          WHERE
                                            QR.QUIZ_ID = Q.QUIZ_ID 
                                            AND Q.FOSTAN_ID = C.FOSTAN_ID 
                                            AND QR.STUDENT_ID = ?");
        $stmt->bind_param("i", $student_id);
        $stmt->execute();

        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($FOSTAN_ID, $ANNOUNCEMENT_DESC);

        $response = array();
        if ($num_of_rows > 0) {
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $ANNOUNCEMENT_DESC;

                array_push($response['data'], $temp);
            }
        } else {

            $response['error'] = true;
            $response['message_en'] = "No Quizes Result Found";
        }
        $stmt->close();
        return $response;
    }

    /*
     * Announcements Table *********************
     */

    //Get All Announcement
    public function getAllAnnouncements($start, $end) {
        $stmt = $this->conn->prepare("SELECT * from announcements "
                . "ORDER BY ANNOUNCEMENT_ID DESC LIMIT ?,?");
        $stmt->bind_param("ii", $start, $end);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($ANNOUNCEMENT_ID, $ANNOUNCEMENT_DESC, $ANNOUNCEMENT_PIC, $ANNOUNCEMENT_CREATED_AT, $USER_ID, $ANNOUNCEMENT_UPDATED_AT);

        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {

                $temp = array();
                $temp['ANNOUNCEMENT_ID'] = $ANNOUNCEMENT_ID;
                $temp['ANNOUNCEMENT_DESC'] = $ANNOUNCEMENT_DESC;
                $temp['ANNOUNCEMENT_PIC'] = $ANNOUNCEMENT_PIC;
                $temp['ANNOUNCEMENT_CREATED_AT'] = $ANNOUNCEMENT_CREATED_AT;
                $temp['USER_ID'] = $USER_ID;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Announcmenets Found";
        }
        $stmt->close();
        return $response;
    }

    //Get All Announcement For Instructor
    public function getAllAnnouncementsForInstructor($start, $end, $user_id) {
        $stmt = $this->conn->prepare("Select * from announcements where USER_ID = ? "
                . "ORDER BY ANNOUNCEMENT_ID DESC LIMIT ?,?");
        $stmt->bind_param("iii", $user_id, $start, $end);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($ANNOUNCEMENT_ID, $ANNOUNCEMENT_DESC, $ANNOUNCEMENT_PIC, $ANNOUNCEMENT_CREATED_AT, $USER_ID, $ANNOUNCEMENT_UPDATED_AT);

        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($row = $stmt->fetch()) {
                $temp = array();
                $temp['ANNOUNCEMENT_ID'] = $ANNOUNCEMENT_ID;
                $temp['ANNOUNCEMENT_DESC'] = $ANNOUNCEMENT_DESC;
                $temp['ANNOUNCEMENT_PIC'] = $ANNOUNCEMENT_PIC;
                $temp['ANNOUNCEMENT_CREATED_AT'] = $ANNOUNCEMENT_CREATED_AT;
                $temp['USER_ID'] = $USER_ID;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Announcmenets Found";
        }

        $stmt->close();
        return $response;
    }

    //Get Announcement By ID
    public function getAnnouncementByID($announcement_id) {
        $stmt = $this->conn->prepare("SELECT * from announcements where ANNOUNCEMENT_ID = ?");
        $stmt->bind_param("i", $announcement_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($ANNOUNCEMENT_ID, $ANNOUNCEMENT_DESC, $ANNOUNCEMENT_PIC, $ANNOUNCEMENT_CREATED_AT, $USER_ID, $ANNOUNCEMENT_UPDATED_AT);

        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {

                $temp = array();
                $temp['ANNOUNCEMENT_ID'] = $ANNOUNCEMENT_ID;
                $temp['ANNOUNCEMENT_DESC'] = $ANNOUNCEMENT_DESC;
                $temp['ANNOUNCEMENT_PIC'] = $ANNOUNCEMENT_PIC;
                $temp['ANNOUNCEMENT_CREATED_AT'] = $ANNOUNCEMENT_CREATED_AT;
                $temp['USER_ID'] = $USER_ID;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Announcmenets Found";
        }
        $stmt->close();
        return $response;
    }

    //create ANNOUNCEMENT
    public function createAnnouncement($ANNOUNCEMENT_DESC, $ANNOUNCEMENT_PIC, $USER_ID) {

        $response = array();

        if (!empty($ANNOUNCEMENT_PIC)) {
            $filePath = $this->DecodeImage($ANNOUNCEMENT_PIC);
            if ($filePath != null) {
                $stmt = $this->conn->prepare(
                        "insert into announcements(ANNOUNCEMENT_DESC,ANNOUNCEMENT_PIC , USER_ID)"
                        . " values (?,?,?)");
                $stmt->bind_param("ssi", $ANNOUNCEMENT_DESC, $filePath, $USER_ID);

                $result = $stmt->execute();
                $announcement_id = $this->conn->insert_id;
                $stmt->close();

                if ($result) {
                    //User Successfully inserted
                    $response["error"] = false;
                    $response['message_en'] = "Announcement has been created successfully";
                    $response["id"] = $announcement_id;
                    $this->commitDB();
                } else {
                    $response["error"] = true;
                    $response['message_en'] = "An error occured while creating the announcement";
                }
            } else {
                $response["error"] = true;
                $response['message_en'] = "Error While Uploading The Image";
            }
        } else {
            $stmt = $this->conn->prepare(
                    "insert into announcements(ANNOUNCEMENT_DESC, USER_ID)"
                    . " values (?,?)");
            $stmt->bind_param("si", $ANNOUNCEMENT_DESC, $USER_ID);

            $result = $stmt->execute();
            $announcement_id = $this->conn->insert_id;
            $stmt->close();

            if ($result) {
                //User Successfully inserted
                $response["error"] = false;
                $response['message_en'] = "Announcement has been created successfully";
                $response["id"] = $announcement_id;
                $this->commitDB();
            } else {
                $response["error"] = true;
                $response['message_en'] = "An error occured while creating the announcement";
            }
        }

        return $response;
    }

    //DELETE Announcement
    public function deleteAnnouncement($ANNOUNCEMENT_ID) {
        $response = array();
        $stmt = $this->conn->prepare("DELETE FROM announcements WHERE ANNOUNCEMENT_ID = ? ");
        $stmt->bind_param("i", $ANNOUNCEMENT_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            // User successfully updated
            $response["error"] = false;
            $response['message_en'] = 'ANNOUNCEMENT Has Been Deleted Successfully';
            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to delete the ANNOUNCEMENT";
            $stmt->error;
        }

        return $response;
    }

    //UPDATE Announcement
    public function updateAnnouncement($ANNOUNCEMENT_ID, $ANNOUNCEMENT_DESC, $ANNOUNCEMENT_PIC) {
        $response = array();
        if (!empty($ANNOUNCEMENT_PIC) && $ANNOUNCEMENT_PIC != "" && !empty($ANNOUNCEMENT_DESC) && $ANNOUNCEMENT_DESC != "") {
            $filePath = $this->DecodeImage($ANNOUNCEMENT_PIC);
            if ($filePath != null) {
                $stmt = $this->conn->prepare("UPDATE announcements SET ANNOUNCEMENT_DESC = ? , ANNOUNCEMENT_PIC  = ? ,ANNOUNCEMENT_CREATED_AT = now()"
                        . " WHERE ANNOUNCEMENT_ID = ?   ");
                $stmt->bind_param("ssi", $ANNOUNCEMENT_DESC, $filePath, $ANNOUNCEMENT_ID);
                $stmt->execute();
                $stmt->store_result();
                $num_rows = $stmt->affected_rows;

                if ($num_rows > 0) {
                    // User successfully updated
                    $response["error"] = false;
                    $response['message_en'] = 'Announcement Info has been updated successfully';
                    $this->commitDB();
                } else {
                    // Failed to update user
                    $response["error"] = true;
                    $response['message_en'] = "Failed to update Announcement info";
                }
            } else {
                $response["error"] = true;
                $response['message_en'] = "Failed to update announcement image";
            }
        } else if (empty($ANNOUNCEMENT_PIC) && $ANNOUNCEMENT_PIC == "" && !empty($ANNOUNCEMENT_DESC) && $ANNOUNCEMENT_DESC != "") {
            $stmt = $this->conn->prepare("UPDATE announcements SET ANNOUNCEMENT_DESC = ? , ANNOUNCEMENT_PIC  = '' ,ANNOUNCEMENT_CREATED_AT = now()"
                    . " WHERE ANNOUNCEMENT_ID = ?   ");
            $stmt->bind_param("si", $ANNOUNCEMENT_DESC, $ANNOUNCEMENT_ID);
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->affected_rows;

            if ($num_rows > 0) {
                // User successfully updated
                $response["error"] = false;
                $response['message_en'] = 'Announcement Info has been updated successfully';
                $this->commitDB();
            } else {
                // Failed to update user
                $response["error"] = true;
                $response['message_en'] = "Failed to update Announcement info";
            }
        } else if (!empty($ANNOUNCEMENT_PIC) && $ANNOUNCEMENT_PIC != "" && empty($ANNOUNCEMENT_DESC) && $ANNOUNCEMENT_DESC == "") {
            $filePath = $this->DecodeImage($ANNOUNCEMENT_PIC);
            if ($filePath != null) {
                $stmt = $this->conn->prepare("UPDATE announcements SET  ANNOUNCEMENT_PIC  = ? ,ANNOUNCEMENT_CREATED_AT = now()"
                        . " WHERE ANNOUNCEMENT_ID = ?   ");
                $stmt->bind_param("si", $filePath, $ANNOUNCEMENT_ID);
                $stmt->execute();
                $stmt->store_result();
                $num_rows = $stmt->affected_rows;

                if ($num_rows > 0) {
                    // User successfully updated
                    $response["error"] = false;
                    $response['message_en'] = 'Announcement Info has been updated successfully';
                    $this->commitDB();
                } else {
                    // Failed to update user
                    $response["error"] = true;
                    $response['message_en'] = "Failed to update Announcement info";
                }
            } else {
                $response["error"] = true;
                $response['message_en'] = "Failed to update announcement image";
            }
        }
        return $response;
    }

    /*
     * Assignments Table ***************
     */

    //Create Assignment
    public function createAssignment($ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE, $FOSTAN_ID, $USER_ID) {


        $stmt = $this->conn->prepare(
                "insert into assignments( ASSIGNMENT_TITLE  , ASSIGNMENT_DESC , ASSIGNMENT_DEADLINE , FOSTAN_ID,USER_ID )"
                . " values (?,?,?,?,?)");
        $stmt->bind_param("sssii", $ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE, $FOSTAN_ID, $USER_ID);
        $result = $stmt->execute();
        $stmt->close();

        if ($result)
            return $this->conn->insert_id;
        return null;
    }

    //DELETE Assignment
    public function deleteAssignment($ASSIGNMENT_ID) {
        $response = array();
        $stmt = $this->conn->prepare("DELETE FROM assignments WHERE ASSIGNMENT_ID = ? ");
        $stmt->bind_param("i", $ASSIGNMENT_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            $response["error"] = false;
            $response['message_en'] = 'Assignment Has Been Deleted Successfully';
            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to delete the Assignment";
            $stmt->error;
        }
        return $response;
    }

    //UPDATE ASSIGNMENT
    public function updateAssignment($ASSIGNMENT_ID, $ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE) {
        $response = array();
        if ($ASSIGNMENT_ID != NULL) {
            $stmt = $this->conn->prepare("UPDATE assignments SET ASSIGNMENT_TITLE = ? , ASSIGNMENT_DESC  = ?"
                    . " ,ASSIGNMENT_DEADLINE = ? ,ASSIGNMENT_UPDATED_AT = now() "
                    . " WHERE ASSIGNMENT_ID = ? ");
            $stmt->bind_param("sssi", $ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE, $ASSIGNMENT_ID);
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->affected_rows;
            if ($num_rows > 0) {
                // User successfully updated
                $response["error"] = false;
                $response['message_en'] = 'ASSIGNMENT Info has been updated successfully';
            } else {
                // Failed to update user
                $response["error"] = true;
                $response['message_en'] = "Failed to update ASSIGNMENT info";
                $stmt->error;
            }
            $stmt->close();
        } else {
            $response["error"] = true;
            $response['message_en'] = "An error occured while updating the ASSIGNMENT";
        }
        return $response;
    }

    //Get Instructor Assignments
    public function getInstructorAssignments($start, $end, $fostan_id, $instructor_id) {
        if ($fostan_id == "*") {
            $stmt = $this->conn->prepare("SELECT a.*, c.FOSTAN_NAME FROM
                                            assignments a,
                                            fostans c,
                                            users u
                                          WHERE
                                            a.FOSTAN_ID = c.FOSTAN_ID
                                            AND a.USER_ID = u.USER_ID AND a.USER_ID = ?
                                          ORDER BY
                                            ASSIGNMENT_ID
                                          DESC
                                          LIMIT ?,? ");
            $stmt->bind_param("iii", $instructor_id, $start, $end);
        } else {
            $stmt = $this->conn->prepare("SELECT a.*, c.FOSTAN_NAME FROM
                                            assignments a,
                                            fostans c,
                                            users u
                                          WHERE
                                            a.FOSTAN_ID = c.FOSTAN_ID AND a.FOSTAN_ID = ? And a.ASSIGNMENT_DEADLINE  >= NOW() 
                                            AND a.USER_ID = u.USER_ID AND a.USER_ID = ?
                                          ORDER BY
                                            ASSIGNMENT_ID
                                          DESC
                                          LIMIT ?,? ");
            $stmt->bind_param("iiii", $fostan_id, $instructor_id, $start, $end);
        }

        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($ASSIGNMENT_ID, $ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE, $FOSTAN_ID, $USER_ID, $ASSIGNMENT_CREATED_AT, $ASSIGNMENT_UPDATED_AT, $FOSTAN_NAME);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($row = $stmt->fetch()) {
                $temp = array();
                $temp['ASSIGNMENT_ID'] = $ASSIGNMENT_ID;
                $temp['ASSIGNMENT_TITLE'] = $ASSIGNMENT_TITLE;
                $temp['ASSIGNMENT_DESC'] = $ASSIGNMENT_DESC;
                $temp['ASSIGNMENT_DEADLINE'] = $ASSIGNMENT_DEADLINE;
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['ASSIGNMENT_CREATED_AT'] = $ASSIGNMENT_CREATED_AT;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Assignments Found";
        }
        $stmt->close();
        return $response;
    }

    //Get Student Assignments
    public function getStudentAssignments($start, $end, $fostan_id, $student_id) {
        if ($fostan_id != "*") {
            $stmt = $this->conn->prepare("SELECT a.*, u.USER_FULL_NAME , u.USER_ID , c.FOSTAN_NAME FROM
                                            assignments a,
                                            fostans c,
                                            users u
                                          WHERE
                                            a.FOSTAN_ID = c.FOSTAN_ID AND a.FOSTAN_ID = ? AND a.ASSIGNMENT_DEADLINE >= NOW() 
                                            AND a.USER_ID = u.USER_ID 
                                          ORDER BY
                                            ASSIGNMENT_ID
                                          DESC
                                          LIMIT ?,? ");
            $stmt->bind_param("iii", $fostan_id, $start, $end);
        } else {
            $stmt = $this->conn->prepare("SELECT a.*,u.USER_FULL_NAME , u.USER_ID ,
                                                    c.FOSTAN_NAME
                                                  FROM
                                                    assignments a,
                                                    fostans c,
                                                    users u
                                                  WHERE
                                                    a.FOSTAN_ID = c.FOSTAN_ID AND a.FOSTAN_ID IN(
                                                    SELECT
                                                      cs.fostan_id
                                                    FROM
                                                      fostans_student cs
                                                    WHERE
                                                      cs.student_id = ?
                                                  ) AND a.ASSIGNMENT_DEADLINE >= NOW() AND a.USER_ID = u.USER_ID
                                                  ORDER BY
                                                    ASSIGNMENT_ID
                                                  DESC
                                                  LIMIT ?,?");
            $stmt->bind_param("iii", $student_id, $start, $end);
        }
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($ASSIGNMENT_ID, $ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE, $FOSTAN_ID, $USER_ID, $ASSIGNMENT_CREATED_AT, $ASSIGNMENT_UPDATED_AT, $USER_FULL_NAME, $USER_ID, $FOSTAN_NAME);


        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['ASSIGNMENT_ID'] = $ASSIGNMENT_ID;
                $temp['ASSIGNMENT_TITLE'] = $ASSIGNMENT_TITLE;
                $temp['ASSIGNMENT_DESC'] = $ASSIGNMENT_DESC;
                $temp['INSTRUCTOR_ID'] = $USER_ID;
                $temp['INSTRUCTOR_NAME'] = $USER_FULL_NAME;
                $temp['ASSIGNMENT_DEADLINE'] = $ASSIGNMENT_DEADLINE;
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['ASSIGNMENT_CREATED_AT'] = $ASSIGNMENT_CREATED_AT;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Assignments Found";
        }
        $stmt->close();
        return $response;
    }

    //Get Assignment By ID
    public function getAssignmentByID($assignment_id) {

        $stmt = $this->conn->prepare("SELECT a.*, u.USER_FULL_NAME , u.USER_ID , c.FOSTAN_NAME 
                                                    FROM
                                                            assignments a,
                                                            fostans c,
                                                            users u
                                                      WHERE
                                                            a.FOSTAN_ID = c.FOSTAN_ID
                                                        AND a.ASSIGNMENT_DEADLINE >= NOW() 
                                                            AND a.USER_ID = u.USER_ID 
                                                            AND a.ASSIGNMENT_ID = ? ");
        $stmt->bind_param("i", $assignment_id);

        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($ASSIGNMENT_ID, $ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE, $FOSTAN_ID, $USER_ID, $ASSIGNMENT_CREATED_AT, $ASSIGNMENT_UPDATED_AT, $USER_FULL_NAME, $USER_ID, $FOSTAN_NAME);


        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['ASSIGNMENT_ID'] = $ASSIGNMENT_ID;
                $temp['ASSIGNMENT_TITLE'] = $ASSIGNMENT_TITLE;
                $temp['ASSIGNMENT_DESC'] = $ASSIGNMENT_DESC;
                $temp['INSTRUCTOR_ID'] = $USER_ID;
                $temp['INSTRUCTOR_NAME'] = $USER_FULL_NAME;
                $temp['ASSIGNMENT_DEADLINE'] = $ASSIGNMENT_DEADLINE;
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['ASSIGNMENT_CREATED_AT'] = $ASSIGNMENT_CREATED_AT;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Assignments Found";
        }
        $stmt->close();
        return $response;
    }

    //Get Attachments for the Assignment
    public function getAttachmentsForAssignments($assignment_id) {
        $stmt = $this->conn->prepare("SELECT
                                a.ATTACHMENT_ID,
                                a.ATTACHMENT_NAME,
                                a.ATTACHMENT_URL,
                                aa.aa_id
                              FROM
                                attachments a,
                                assignments assi,
                                assignments_attachments aa
                              WHERE
                                assi.ASSIGNMENT_ID = ? AND 
                                aa.attachment_id = a.ATTACHMENT_ID
                                AND aa.ASSIGNMENT_ID = assi.ASSIGNMENT_ID ");
        $stmt->bind_param("i", $assignment_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($ATTACHMENT_ID, $ATTACHMENT_NAME, $ATTACHMENT_URL, $aa_id);

        $response = array();
        if ($num_of_rows > 0) {
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['ATTACHMENT_ID'] = $ATTACHMENT_ID;
                $temp['ATTACHMENT_NAME'] = $ATTACHMENT_NAME;
                $temp['ATTACHMENT_URL'] = $ATTACHMENT_URL;
                $temp['AA_ID'] = $aa_id;

                array_push($response['data'], $temp);
            }
        } else {

            $response['error'] = true;
            $response['message_en'] = "No Records Found";
        }
        $stmt->close();
        return $response;
    }

    //INSERT ATTACHMENT FOR THE Assignment 
    public function insertAttachmentForAssignment($assignment_id, $attachment_id) {
        $stmt = $this->conn->prepare("insert into assignments_attachments (ASSIGNMENT_ID,ATTACHMENT_ID) values (?,?) ");
        $stmt->bind_param("ii", $assignment_id, $attachment_id);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return $this->conn->insert_id;
        } else {
            return null;
        }
    }

    //Update Attachments for the Assignment
    public function updateAttachmentForAssignment($ASSIGNMENT_ID, $ATTACHMENT_ID, $AA_ID) {
        $response = array();

        $stmt = $this->conn->prepare("UPDATE assignments_attachments SET attachment_id = ? , ASSIGNMENT_ID  = ? "
                . " WHERE aa_id = ? ");
        $stmt->bind_param("iii", $ATTACHMENT_ID, $ASSIGNMENT_ID, $AA_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            return $num_rows;
        } else {
            return null;
        }
        $stmt->close();


        return $response;
    }

    //Delete Attachments for the Assignment
    public function removeAttachmentForAssignment($aa_id) {

        $response = array();
        $stmt = $this->conn->prepare("delete from assignments_attachments where aa_id = ?");
        $stmt->bind_param("i", $aa_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            return $num_rows;
        } else {
            return null;
        }
    }

    /**
     * Quiz Table **************
     */
    //Create Quiz
    public function createQuiz($QUIZ_NAME, $USER_ID, $FOSTAN_ID) {

        $response = array();
        //insert query
        $stmt = $this->conn->prepare(
                "insert into quiz( QUIZ_NAME  , USER_ID  , FOSTAN_ID)"
                . " values (?,?,?)");
        $stmt->bind_param("sii", $QUIZ_NAME, $USER_ID, $FOSTAN_ID);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            //User Successfully inserted
            $response["error"] = false;
            $response['message_en'] = "Quiz has been created successfully";
            $this->commitDB();
        } else {
            $response["error"] = true;
            $response['message_en'] = "An error occured while creating the quiz";
        }
        return $response;
    }

    //DELETE Quiz
    public function deleteQuiz($QUIZ_ID) {
        $response = array();
        $stmt = $this->conn->prepare("DELETE FROM quiz WHERE QUIZ_ID = ? ");
        $stmt->bind_param("i", $QUIZ_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            $response["error"] = false;
            $response['message_en'] = 'Quiz Has Been Deleted Successfully';
            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to delete the quiz";
            $stmt->error;
        }
        return $response;
    }

    //UPDATE Quiz
    public function updateQuiz($QUIZ_ID, $QUIZ_NAME, $FOSTAN_ID) {
        $response = array();
        $stmt = $this->conn->prepare("UPDATE quiz SET QUIZ_NAME  = ? , FOSTAN_ID = ? "
                . " WHERE QUIZ_ID = ? ");
        $stmt->bind_param("sii", $QUIZ_NAME, $FOSTAN_ID, $QUIZ_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            // User successfully updated
            $response["error"] = false;
            $response['message_en'] = 'Quiz Info has been updated successfully';
            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to update quiz info";
            $stmt->error;
        }
        $stmt->close();

        return $response;
    }

    //Get All Quizs For Instructor
    public function getAllQuizsForInstructor($start, $end, $fostan_id, $user_id) {
        if ($fostan_id != "*") {
            $stmt = $this->conn->prepare("SELECT
                                                Q.* , C.FOSTAN_NAME , C.FOSTAN_ID 
                                              FROM
                                                quiz Q , fostans C
                                              WHERE
                                                Q.FOSTAN_ID = ? AND Q.USER_ID = ? AND C.FOSTAN_ID = Q.FOSTAN_ID
                                              ORDER BY
                                                Q.QUIZ_ID
                                              DESC
                                              LIMIT ?, ?");
            $stmt->bind_param("iiii", $fostan_id, $user_id, $start, $end);
            $stmt->execute();
            $stmt->store_result();
            $num_of_rows = $stmt->num_rows;
            $stmt->bind_result($QUIZ_ID, $QUIZ_NAME, $USER_ID, $FOSTAN_ID, $QUIZ_CREATED_AT, $QUIZ_UPDATED_AT, $FOSTAN_NAME, $FOSTAN_ID);
        } else {
            $stmt = $this->conn->prepare("SELECT
                                                Q.*, C.FOSTAN_NAME
                                              FROM
                                                quiz Q, fostans C
                                              WHERE
                                                Q.USER_ID = ? AND C.FOSTAN_ID = Q.FOSTAN_ID
                                              ORDER BY
                                                Q.QUIZ_ID
                                              DESC
                                              LIMIT ?, ?");
            $stmt->bind_param("iii", $user_id, $start, $end);
            $stmt->execute();
            $stmt->store_result();
            $num_of_rows = $stmt->num_rows;
            $stmt->bind_result($QUIZ_ID, $QUIZ_NAME, $USER_ID, $FOSTAN_ID, $QUIZ_CREATED_AT, $QUIZ_UPDATED_AT, $FOSTAN_NAME);
        }
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['QUIZ_ID'] = $QUIZ_ID;
                $temp['QUIZ_NAME'] = $QUIZ_NAME;
                $temp['USER_ID'] = $USER_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['QUIZ_CREATED_AT'] = $QUIZ_CREATED_AT;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    //Get All Quizs For Student
    public function getAllQuizsForStudent($start, $end, $fostan_id, $user_id) {
        if ($fostan_id != "*") {
            $stmt = $this->conn->prepare("SELECT
                                            Q.*,
                                            U.USER_FULL_NAME, C.FOSTAN_NAME , C.FOSTAN_ID
                                          FROM
                                            quiz Q,
                                            users U , fostans C
                                          WHERE
                                            Q.FOSTAN_ID = ? AND U.USER_ID = Q.USER_ID AND C.FOSTAN_ID = Q.FOSTAN_ID AND (
                                            SELECT
                                              COUNT(*)
                                            FROM
                                              quiz_result qr
                                            WHERE
                                              qr.STUDENT_ID = ? AND qr.QUIZ_ID = Q.QUIZ_ID
                                          ) = 0 AND (SELECT COUNT(*) FROM questions_quiz WHERE Quiz_ID = Q.QUIZ_ID) > 0
                                          ORDER BY
                                            Q.QUIZ_ID
                                          DESC
                                          LIMIT ?,?");
            $stmt->bind_param("iiii", $fostan_id, $user_id, $start, $end);
        } else {
            $stmt = $this->conn->prepare("SELECT
                                            Q.*,
                                            U.USER_FULL_NAME , C.FOSTAN_NAME, C.FOSTAN_ID
                                          FROM
                                            quiz Q,
                                            users U , fostans C
                                          WHERE
                                             U.USER_ID = Q.USER_ID AND C.FOSTAN_ID = Q.FOSTAN_ID AND (
                                            SELECT
                                              COUNT(*)
                                            FROM
                                              quiz_result qr
                                            WHERE
                                              qr.STUDENT_ID = ? AND qr.QUIZ_ID = Q.QUIZ_ID
                                          ) = 0 AND (SELECT COUNT(*) FROM questions_quiz WHERE Quiz_ID = Q.QUIZ_ID) > 0
                                          ORDER BY
                                            Q.QUIZ_ID
                                          DESC
                                          LIMIT ?,?");
            $stmt->bind_param("iii", $user_id, $start, $end);
        }

        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($QUIZ_ID, $QUIZ_NAME, $USER_ID, $FOSTAN_ID, $QUIZ_CREATED_AT, $QUIZ_UPDATED_AT, $USER_FULL_NAME, $FOSTAN_NAME, $FOSTAN_ID);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['QUIZ_ID'] = $QUIZ_ID;
                $temp['QUIZ_NAME'] = $QUIZ_NAME;
                $temp['USER_ID'] = $USER_ID;
                $temp['FOSTAN_NAME'] = $FOSTAN_NAME;
                $temp['FOSTAN_ID'] = $FOSTAN_ID;
                $temp['USER_FULL_NAME'] = $USER_FULL_NAME;
                $temp['QUIZ_CREATED_AT'] = $QUIZ_CREATED_AT;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    //Get All Quizs For The Fostan
    public function getAllQuizsForFostan($fostan_id) {

        $stmt = $this->conn->prepare("SELECT Q.QUIZ_ID,Q.QUIZ_NAME FROM 
                                            quiz Q 
                                            WHERE
                                            Q.FOSTAN_ID = ?");
        $stmt->bind_param("i", $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($QUIZ_ID, $QUIZ_NAME);

        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['QUIZ_ID'] = $QUIZ_ID;
                $temp['QUIZ_NAME'] = $QUIZ_NAME;
                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    /*
     *  Questions Quiz Table *********************
     */

    //Create QQ
    public function createQQ($QQ_TEXT, $QQ_A1, $QQ_A2, $QQ_A3, $QQ_A4, $QQ_RA, $Quiz_ID) {

        $response = array();
        //insert query
        $stmt = $this->conn->prepare(
                "insert into questions_quiz( QQ_TEXT  , QQ_A1 , QQ_A2 ,QQ_A3, QQ_A4, QQ_RA, QUIZ_ID)"
                . " values (?,?,?,?,?,?,?)");
        $stmt->bind_param("sssssii", $QQ_TEXT, $QQ_A1, $QQ_A2, $QQ_A3, $QQ_A4, $QQ_RA, $Quiz_ID);

        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            //User Successfully inserted
            $response["error"] = false;
            $response['message_en'] = "Question has been created successfully";
            $this->commitDB();
        } else {
            $response["error"] = true;
            $response['message_en'] = "An error occured while creating the Question";
        }
        return $response;
    }

    //DELETE QQ
    public function deleteQQ($QQ_ID) {
        $response = array();
        $stmt = $this->conn->prepare("DELETE FROM questions_quiz WHERE QQ_ID = ? ");
        $stmt->bind_param("i", $QQ_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            $response["error"] = false;
            $response['message_en'] = 'Question Has Been Deleted Successfully';
            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to delete the question";
            $stmt->error;
        }
        return $response;
    }

    //UPDATE QQ
    public function updateQQ($QQ_ID, $QQ_TEXT, $QQ_A1, $QQ_A2, $QQ_A3, $QQ_A4, $QQ_RA) {
        $response = array();
        $stmt = $this->conn->prepare("UPDATE questions_quiz SET QQ_TEXT  = ? , QQ_A1 = ? , QQ_A2 = ? , QQ_A3 = ? , QQ_A4 = ? , QQ_RA = ? "
                . " WHERE QQ_ID = ? ");
        $stmt->bind_param("sssssii", $QQ_TEXT, $QQ_A1, $QQ_A2, $QQ_A3, $QQ_A4, $QQ_RA, $QQ_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            // User successfully updated
            $response["error"] = false;
            $response['message_en'] = 'Question Info has been updated successfully';
            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to update question info";
            $stmt->error;
        }
        $stmt->close();

        return $response;
    }

    //Get All QQ Student
    public function getQQStudent($quiz_id) {
        $stmt = $this->conn->prepare("SELECT * from questions_quiz qq , quiz q "
                . "where qq.quiz_id = ? "
                . "and qq.quiz_id = q.quiz_id ");
        $stmt->bind_param("i", $quiz_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($QQ_ID, $QQ_TEXT, $QQ_A1, $QQ_A2, $QQ_A3, $QQ_A4, $QQ_RA, $Quiz_ID, $QUIZ_ID, $QUIZ_NAME, $USER_ID, $FOSTAN_ID, $QUIZ_CREATED_AT, $QUIZ_UPDATED_AT);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['QQ_ID'] = $QQ_ID;
                $temp['QQ_TEXT'] = $QQ_TEXT;
                $temp['QQ_A1'] = $QQ_A1;
                $temp['QQ_A2'] = $QQ_A2;
                $temp['QQ_A3'] = $QQ_A3;
                $temp['QQ_A4'] = $QQ_A4;
                $temp['QQ_RA'] = $QQ_RA;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    //Get All QQ Instructor
    public function getQQInstructor($start, $end, $quiz_id) {
        $stmt = $this->conn->prepare("SELECT * from questions_quiz qq , quiz q "
                . "where qq.quiz_id = ? "
                . "and qq.quiz_id = q.quiz_id "
                . "LIMIT ?,?");
        $stmt->bind_param("iii", $quiz_id, $start, $end);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($QQ_ID, $QQ_TEXT, $QQ_A1, $QQ_A2, $QQ_A3, $QQ_A4, $QQ_RA, $Quiz_ID, $QUIZ_ID, $QUIZ_NAME, $USER_ID, $FOSTAN_ID, $QUIZ_CREATED_AT, $QUIZ_UPDATED_AT);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['QQ_ID'] = $QQ_ID;
                $temp['QQ_TEXT'] = $QQ_TEXT;
                $temp['QQ_A1'] = $QQ_A1;
                $temp['QQ_A2'] = $QQ_A2;
                $temp['QQ_A3'] = $QQ_A3;
                $temp['QQ_A4'] = $QQ_A4;
                $temp['QQ_RA'] = $QQ_RA;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    /*
     * Quiz Results Table **************
     */

    //Create Quiz Results
    public function createQuizResult($QR_MARK, $QUIZ_ID, $STUDENT_ID) {

        $response = array();
        //insert query
        $stmt = $this->conn->prepare(
                "insert into quiz_result (QR_MARK ,QUIZ_ID,STUDENT_ID) values (?,?,?)");
        $stmt->bind_param("iii", $QR_MARK, $QUIZ_ID, $STUDENT_ID);

        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            //User Successfully inserted
            $response["error"] = false;
            $response['message_en'] = "Quiz Result has been created successfully";
            $this->commitDB();
        } else {
            $response["error"] = true;
            $response['message_en'] = "An error occured while creating the Quiz Result";
        }
        return $response;
    }

    //Get Quiz Result for student
    public function getQuizResultsForStudent($quiz_id, $student_id) {
        $stmt = $this->conn->prepare("SELECT QR_MARK FROM quiz_result   "
                . "WHERE QUIZ_ID = ? AND "
                . "STUDENT_ID = ? ");
        $stmt->bind_param("ii", $quiz_id, $student_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($QR_MARK);

        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;

            while ($stmt->fetch()) {
                $response['mark'] = $QR_MARK;
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "You didn't submit for this quiz";
        }
        $stmt->close();
        return $response;
    }

    /*
     * Grads Table **************
     */

    //GET FOSTAN BY ID
    public function getFostanNameByID($id) {
        $stmt = $this->conn->prepare("select c.FOSTAN_NAME from fostans c where c.FOSTAN_ID = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($FOSTAN_NAME);

        $stmt->close();
        if ($num_of_rows > 0) {
            return $FOSTAN_NAME;
        }
        return null;
    }

    //Create Grades
    public function createGrades($JSON_MARKS) {

        $response = array();
        $inserted = false;
        //insert query
        $MARKS = json_decode($JSON_MARKS);

        $x = 0;
        foreach ($MARKS as $student) {
            $stmt = $this->conn->prepare("insert into student_marks(STUDENT_ID,SM_MARK ,FOSTAN_ID) values (?,?,?)");
            $stmt->bind_param("iii", $student->STUDENT_ID, $student->SM_MARK, $student->FOSTAN_ID);
            $fostan_id = $student->FOSTAN_ID;

            //this condition for prevent get the fostanByID invoked each time in for each
            if ($x == 0) {
                $fostan_name = $this->getFostanNameByID($fostan_id);
            } else {
                $x++;
            }

            if ($stmt->execute()) {
                $inserted = true;
                $this->sendNewCreationNotification(PUSH_FLAG_CREATE_GRADE, $fostan_id, $fostan_name, $student->STUDENT_ID, null);
            } else {
                $inserted = false;
                break;
            }
        }
        if ($inserted) {
            //User Successfully inserted
            $response["id"] = $fostan_id;
            $response["error"] = false;
            $response['message_en'] = "Grades has been created successfully";
            $this->commitDB();
        } else {
            $response["error"] = true;
            $response['message_en'] = "Grades not created, please try again";
            $this->rollbackDB();
        }
        $stmt->close();
        return $response;
    }

    //UPDATE Grades
    public function updateGrades($GRADE_MARK, $FOSTAN_ID, $STUDENT_ID) {
        $response = array();
        $stmt = $this->conn->prepare("UPDATE grades SET GRADE_MARK  = ?  "
                . " WHERE FOSTAN_ID = ? and STUDENT_ID = ? ");
        $stmt->bind_param("iii", $GRADE_MARK, $FOSTAN_ID, $STUDENT_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            // User successfully updated
            $response["error"] = false;
            $response['message_en'] = 'Grades Info has been updated successfully';
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to update grade ";
            $stmt->error;
        }
        $stmt->close();

        return $response;
    }

    //is grades filled for the fostan
    private function isGradesFilled($fostan_id) {
        $stmt = $this->conn->prepare("SELECT GRADE_MARK from grades WHERE FOSTAN_ID = ?");
        $stmt->bind_param("i", $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    //Get Quiz Result for student
    public function getStudentGrade($fostan_id, $student_id) {
        $stmt = $this->conn->prepare("SELECT SM_MARK FROM student_marks WHERE STUDENT_ID = ? AND FOSTAN_ID = ?");
        $stmt->bind_param("ii", $student_id, $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($SM_MARK);

        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            while ($stmt->fetch()) {
                $response['grade'] = $SM_MARK;
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Result";
        }
        $stmt->close();
        return $response;
    }

    /*     * **** ******************* CHAT Discussion ********************** */

    //Get Conversation For User
    public function getConversationForUser($user_id) {
        $stmt = $this->conn->prepare("select msg_id,msg_from,msg_to,msg_content,msg_status,msg_created_at,
                            (select user_full_name from users where user_id = case when msg_from = ? then msg_to else msg_from  end ) user_name 
                            from messages where msg_id in 
                            (select max(msg_id) from messages  
                            where (msg_to = ? or msg_from = ?) and msg_chat_room_id is null
                            group by case when msg_from = ? then msg_to else msg_from end ) order by msg_created_at desc ");
        $stmt->bind_param("iiii", $user_id, $user_id, $user_id, $user_id); 
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($msg_id, $msg_from, $msg_to, $msg_content, $msg_status, $msg_created_at, $user_name);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['msg_id'] = $msg_id;
                $temp['msg_content'] = $msg_content;
                $temp['msg_to'] = $msg_to;
                $temp['msg_from'] = $msg_from;
                $temp['msg_status'] = $msg_status;
                $temp['msg_created_at'] = $msg_created_at;
                $temp['user_name'] = $user_name;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    //Get Conversation From User
    public function getConversationFromUser($user_from, $user_to, $startItem, $endItem) {
        $stmt = $this->conn->prepare("select  CASE WHEN msg_from = ? 
            then msg_to else msg_from 
                  end msg_from_result , msg_id,msg_from,msg_to,msg_content,msg_status,msg_created_at,
            (select user_full_name from users where user_id = msg_from_result) user_name
                   from messages 
            where ((msg_from = ? and msg_to = ?) or  (msg_from = ? and msg_to = ?) )
                order by msg_id desc limit ? , ? ");
        $stmt->bind_param("iiiiiii", $user_to, $user_from, $user_to, $user_to, $user_from, $startItem, $endItem);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($msg_from_result, $msg_id, $msg_from, $msg_to, $msg_content, $msg_status, $msg_created_at, $user_name);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['msg_id'] = $msg_id;
                $temp['msg_content'] = $msg_content;
                $temp['msg_from'] = $msg_from;
                $temp['msg_created_at'] = $msg_created_at;
                $temp['fromUserName'] = $user_name;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    // messaging to persional message Discussion
    public function addMessage($from_user_id, $to_user_id, $message) {
        $response = array();

        $stmt = $this->conn->prepare("INSERT INTO messages (msg_from, msg_to, msg_content) values(?, ?, ?)");
        $stmt->bind_param("iis", $from_user_id, $to_user_id, $message);

        $result = $stmt->execute();
        $message_id = $this->conn->insert_id;

        if ($result && $message_id > 0) {
            $response['error'] = false;
            // get the message
            $stmt = $this->conn->prepare("SELECT *,(select user_full_name from users where user_id = ?) fromUserName FROM messages WHERE msg_id = ?");
            $stmt->bind_param("ii", $from_user_id, $message_id);
            if ($stmt->execute()) {
                $stmt->bind_result($msg_id, $msg_from, $msg_to, $msg_chat_room_id, $msg_content, $msg_status, $msg_created_at, $fromUserName);
                $stmt->fetch();
                $tmp = array();
                $tmp['msg_id'] = $msg_id;
                $tmp['msg_from'] = $msg_from;
                $tmp['msg_to'] = $msg_to;
                $tmp['msg_chat_room_id'] = $msg_chat_room_id;
                $tmp['msg_content'] = $msg_content;
                $tmp['msg_status'] = $msg_status;
                $tmp['msg_created_at'] = $msg_created_at;
                $tmp['fromUserName'] = $fromUserName;

                $response['message_en'] = $tmp;
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = 'Failed send message';
        }

        return $response;
    }

    //get Instructors For Student
    public function getInstructorsForStudent($user_id) {
        $stmt = $this->conn->prepare("select distinct u.user_id, u.USER_FULL_NAME from users u , fostans c , fostans_student cs
                                                where u.user_id = c.INSTRUCTOR_ID and
                                                c.FOSTAN_ID = cs.fostan_id and
                                                cs.student_id = ? ");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($_USER_ID, $USER_FULL_NAME);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['USER_ID'] = $_USER_ID;
                $temp['USER_FULL_NAME'] = $USER_FULL_NAME;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "You didn't join for any fostans";
        }
        $stmt->close();
        return $response;
    }

    //get Students FOR Instructors
    public function getStudentsForInstructor($user_id, $fostan_id) {
        if ($fostan_id != "*") {
            $stmt = $this->conn->prepare("select distinct u.user_id, u.USER_FULL_NAME from users u , fostans c , fostans_student cs
                                                where u.USER_ID = cs.student_id and
                                                c.FOSTAN_ID = cs.fostan_id and
                                                c.INSTRUCTOR_ID = ? and c.FOSTAN_ID = ? ");
            $stmt->bind_param("ii", $user_id, $fostan_id);
        } else {
            $stmt = $this->conn->prepare("select distinct u.user_id, u.USER_FULL_NAME from users u , fostans c , fostans_student cs
                                                where u.USER_ID = cs.student_id and
                                                c.FOSTAN_ID = cs.fostan_id and
                                                c.INSTRUCTOR_ID = ? ");
            $stmt->bind_param("i", $user_id);
        }
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($_USER_ID, $USER_FULL_NAME);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['USER_ID'] = $_USER_ID;
                $temp['USER_FULL_NAME'] = $USER_FULL_NAME;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "You don't have joined students";
        }
        $stmt->close();
        return $response;
    }

    /*     * **** ******************* CHAT Collaboration ********************** */

    //Get Chat Rooms For Fostan
    public function getChatRoomsForFostan($fostan_id) {
        if ($fostan_id != "*") {
            $stmt = $this->conn->prepare("select  cr.CHAT_ROOM_ID ,cr.CHAT_ROOM_QUESTION ,cr.CHAT_ROOM_CREATED_AT,
                                        (select USER_FULL_NAME from users where USER_ID= cr.CHAT_ROOM_CREATED_BY ) user_name
                                       from chat_rooms cr 
                                       where cr.FOSTAN_ID = ?");
            $stmt->bind_param("i", $fostan_id);
        } else {
            $stmt = $this->conn->prepare("select  cr.CHAT_ROOM_ID ,cr.CHAT_ROOM_QUESTION ,cr.CHAT_ROOM_CREATED_AT,
                                        (select USER_FULL_NAME from users where USER_ID= cr.CHAT_ROOM_CREATED_BY ) user_name
                                       from chat_rooms cr");
        }
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($CHAT_ROOM_ID, $CHAT_ROOM_QUESTION, $CHAT_ROOM_CREATED_AT, $user_name);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['CHAT_ROOM_ID'] = $CHAT_ROOM_ID;
                $temp['CHAT_ROOM_QUESTION'] = $CHAT_ROOM_QUESTION;
                $temp['CHAT_ROOM_CREATED_AT'] = $CHAT_ROOM_CREATED_AT;
                $temp['user_name'] = $user_name;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    // messaging in a chat room / to persional message
    public function addChatRoom($CHAT_ROOM_QUESTION, $FOSTAN_ID, $CHAT_ROOM_CREATED_BY) {
        $response = array();

        $stmt = $this->conn->prepare("insert into chat_rooms ( CHAT_ROOM_QUESTION,FOSTAN_ID,CHAT_ROOM_CREATED_BY ) values (?,?,?);");
        $stmt->bind_param("sii", $CHAT_ROOM_QUESTION, $FOSTAN_ID, $CHAT_ROOM_CREATED_BY);

        $result = $stmt->execute();
        $chat_room_id = $this->conn->insert_id;

        if ($result && $chat_room_id > 0) {
            $response['error'] = false;
            $response['message_en'] = 'Chat Room Has Been Added Successfully';
            $response['id'] = $chat_room_id;
        } else {
            $response['error'] = true;
            $response['message_en'] = 'Failed send message';
        }

        return $response;
    }

    //Get Conversation From User
    public function getConversationForChatRoom($CHAT_ROOM_ID, $start, $end) {
        $stmt = $this->conn->prepare("select m.* , cr.CHAT_ROOM_CREATED_BY , (select user_full_name from users where user_id = m.msg_from) fromUserName
                                        from messages m , chat_rooms cr
                                        where m.msg_chat_room_id = cr.CHAT_ROOM_ID
                                        and m.msg_chat_room_id = ?
                                        order by m.msg_created_at desc limit ?,?");
        $stmt->bind_param("iii", $CHAT_ROOM_ID, $start, $end);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($msg_id, $msg_from, $msg_to, $msg_chat_room_id, $msg_content, $msg_status, $msg_created_at, $CHAT_ROOM_CREATED_BY, $user_name);
        if ($num_of_rows > 0) {
            $response = array();
            $response['error'] = false;
            $response['data'] = array();

            while ($stmt->fetch()) {
                $temp = array();
                $temp['msg_id'] = $msg_id;
                $temp['msg_content'] = $msg_content;
                $temp['msg_from'] = $msg_from;
                $temp['msg_created_at'] = $msg_created_at;
                $temp['fromUserName'] = $user_name;

                array_push($response['data'], $temp);
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Records Found !";
        }
        $stmt->close();
        return $response;
    }

    // messaging to persional message Discussion
    public function addMessageCollaboration($from_user_id, $chat_room_id, $message) {
        $response = array();

        $stmt = $this->conn->prepare("INSERT INTO messages (msg_from, msg_chat_room_id, msg_content) values(?, ?, ?)");
        $stmt->bind_param("iis", $from_user_id, $chat_room_id, $message);

        $result = $stmt->execute();
        $message_id = $this->conn->insert_id;

        if ($result && $message_id > 0) {
            $response['error'] = false;
            // get the message
            $stmt = $this->conn->prepare("SELECT m.*,(select user_full_name from users where user_id = ?) fromUserName ,cr.CHAT_ROOM_QUESTION
                                            FROM messages m,chat_rooms cr
                                             WHERE msg_id = ?
                                             and m.msg_chat_room_id = cr.CHAT_ROOM_ID");
            $stmt->bind_param("ii", $from_user_id, $message_id);
            if ($stmt->execute()) {
                $stmt->bind_result($msg_id, $msg_from, $msg_to, $msg_chat_room_id, $msg_content, $msg_status, $msg_created_at, $fromUserName, $CHAT_ROOM_QUESTION);
                $stmt->fetch();
                $tmp = array();
                $tmp['msg_id'] = $msg_id;
                $tmp['msg_from'] = $msg_from;
                $tmp['msg_to'] = $msg_to;
                $tmp['msg_chat_room_id'] = $msg_chat_room_id;
                $tmp['msg_content'] = $msg_content;
                $tmp['msg_status'] = $msg_status;
                $tmp['msg_created_at'] = $msg_created_at;
                $tmp['fromUserName'] = $fromUserName;
                $tmp['CHAT_ROOM_QUESTION'] = $CHAT_ROOM_QUESTION;

                $response['message_en'] = $tmp;
            }
        } else {
            $response['error'] = true;
            $response['message_en'] = 'Failed send message';
        }

        return $response;
    }

    public function getUsersForChatRoom($chat_room_id) {

        $users = array();

        $stmt = $this->conn->prepare("select distinct u.registration_id from users u , messages m
                                        where m.msg_from = u.USER_ID 
                                        and m.msg_chat_room_id = ?");
        $stmt->bind_param("i", $chat_room_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($registration_id);
        if ($num_of_rows > 0) {
            while ($stmt->fetch()) {
                $temp = array();
                $temp['registration_id'] = $registration_id;
                array_push($users, $temp);
            }
            $stmt->close();
            return $users;
        }
        $stmt->close();
        return null;
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isEmailExist($email) {
        $stmt = $this->conn->prepare("SELECT USER_ID from users WHERE user_email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    private function isUsernameExists($user_name) {
        $stmt = $this->conn->prepare("SELECT USER_ID from users WHERE user_name = ?");
        $stmt->bind_param("i", $user_name);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT user_id,  user_name, user_email,  user_mobile , user_image , user_country_id , user_gender FROM users WHERE user_email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($USER_ID, $user_name, $user_email, $user_mobile, $user_pic, $user_country_id, $user_gender);
            $stmt->fetch();
            $user = array();
            $user["USER_ID"] = $USER_ID;
            $user["USER_NAME"] = $user_name;
            $user["USER_EMAIL"] = $user_email;
            $user["USER_PIC"] = $user_pic;
            $user["USER_MOBILE"] = $user_mobile;
            $user["USER_COUNTRY_ID"] = $user_country_id;
            $user["USER_GENDER"] = $user_gender;

            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    //UPLOAD ATTACHMENTS
    public function uploadAttachments($file_name, $file_tmp_name) {
        //strtotime("+1 days") Add 1 day because server time
        $now = explode(":", Date('Y:M:d', strtotime("+1 days")));
        if (!file_exists("../uploadAttachments/$now[0]/$now[1]/$now[2]")) {
            mkdir("../uploadAttachments/$now[0]/$now[1]/$now[2]", 0777, true);
        }
        $file_path = "../uploadAttachments/$now[0]/$now[1]/$now[2]/";
        $file_name_orginal = basename($file_name);
        $file_name = date('Y_m_d_H-i-s') . "_" . $file_name_orginal;
        $file_path = $file_path . $file_name;
        if (move_uploaded_file($file_tmp_name, $file_path)) {
            //insert query
            $stmt = $this->conn->prepare("insert into attachments(ATTACHMENT_NAME  , ATTACHMENT_URL )"
                    . " values (?,?) ");
            $stmt->bind_param("ss", $file_name_orginal, $file_path);
            $result = $stmt->execute();
            $stmt->close();
            if ($result) {
                return $this->conn->insert_id;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    //INSERT ATTACHMENT FOR THE FOSTAN 
    public function insertAttachmentForFostan($fostan_id, $attachment_id) {
        $stmt = $this->conn->prepare("insert into fostans_attachments (attachment_id,fostan_id) values (?,?) ");
        $stmt->bind_param("ii", $attachment_id, $fostan_id);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return $this->conn->insert_id;
        } else {
            return null;
        }
    }

    //Decode Image
    public function DecodeImage($USER_PIC) {
        //strtotime("+1 days") Add 1 day because server time
        if (!empty($USER_PIC)) {
            $now = explode(":", Date('Y:M:d', strtotime("+1 days")));
            if (!file_exists("../uploadPhotos/$now[0]/$now[1]/$now[2]")) {
                mkdir("../uploadPhotos/$now[0]/$now[1]/$now[2]", 0777, true);
            }
            $file_path = "../uploadPhotos/$now[0]/$now[1]/$now[2]/";
            $file_name = date('Y_m_d_H-i-s') . "-" . uniqid() . ".JPG";
            $file_path = $file_path . $file_name;
            $binary = base64_decode($USER_PIC);
            $file = fopen($file_path, 'wb');
            if (fwrite($file, $binary)) {
                fclose($file);
                return $file_path;
            } else {
                return null;
            }
        }
        return null;
    }

    //Decode Base64 photo
    public function getImageURL($base) {
        $new_image_name = date('Y-m-d-H-i-s') . '_' . uniqid() . '.jpg';
        $file_path = "uploadPhotos/" . $new_image_name;
        $binary = base64_decode($base);
        $file = fopen($file_path, 'wb');
        fwrite($file, $binary);
        fclose($file);

        return $file_path;
    }

    public function sendNewCreationNotification($CREATION_FOR, $PARAM1, $PARAM2, $PARAM3, $PARAM4) {

        require_once __DIR__ . '/../libs/gcm/gcm.php';
        require_once __DIR__ . '/../libs/gcm/push.php';

        $gcm = new GCM();
        $push = new Push();

        $data = array();

        switch ($CREATION_FOR) {
            case PUSH_FLAG_CREATE_CHATROOM:
                $users = $this->getUsersJoinedForFostanWithCreator($PARAM1);
                $registration_ids = array();
                // preparing gcm registration ids array

                if ($users != null) {
                    foreach ($users as $u) {
                        array_push($registration_ids, $u['registration_id']);
                    }
                } else {
                    $registration_ids = null;
                }

                $data['id'] = $PARAM4;
                $data['CHAT_ROOM_QUESTION'] = $PARAM2;
                $data['fostan_id'] = $PARAM4;
                $data['createdBy'] = $PARAM3;
                $data['message'] = "New collaboration question has been created: " . $PARAM2;
                $push->setFlag(PUSH_FLAG_CREATE_CHATROOM);
                break;

            case PUSH_FLAG_CREATE_FOSTANS:
                $users = $this->getStudentForDept($PARAM1);
                $registration_ids = array();
                // preparing gcm registration ids array
                foreach ($users as $u) {
                    array_push($registration_ids, $u['registration_id']);
                }

                $data['id'] = $PARAM4;
                $data['createdBy'] = $PARAM3;
                $data['message'] = "New fostan has been created: " . $PARAM2;
                $push->setFlag(PUSH_FLAG_CREATE_FOSTANS);
                break;

            case PUSH_FLAG_CREATE_ASSIGNMENT:
                $users = $this->getUsersJoinedForFostan($PARAM1);
                $registration_ids = array();
                // preparing gcm registration ids array

                if ($users != null) {
                    foreach ($users as $u) {
                        array_push($registration_ids, $u['registration_id']);
                    }
                } else {
                    $registration_ids = null;
                }

                $data['id'] = $PARAM4;
                $data['createdBy'] = $PARAM3;
                $data['message'] = "New assignment has been created: " . $PARAM2;
                $push->setFlag(PUSH_FLAG_CREATE_ASSIGNMENT);
                break;

            case PUSH_FLAG_CREATE_ANNOUNCEMENT:
                $users = $this->getAllUsers();
                $registration_ids = array();
                // preparing gcm registration ids array
                if ($users != null) {
                    foreach ($users as $u) {
                        array_push($registration_ids, $u['registration_id']);
                    }
                } else {
                    $registration_ids = null;
                }

                $data['id'] = $PARAM4;
                $data['createdBy'] = $PARAM3;
                $data['message'] = "New announcement has been created: " . $PARAM2;
                $push->setFlag(PUSH_FLAG_CREATE_ANNOUNCEMENT);
                break;

//            case PUSH_FLAG_CREATE_QUIZ:
//                $users = $this->getUsersJoinedForFostan($PARAM1);
//                $registration_ids = array();
//                // preparing gcm registration ids array
//                if ($users != null) {
//                    foreach ($users as $u) {
//                        array_push($registration_ids, $u['registration_id']);
//                    }
//                } else {
//                    $registration_ids = null;
//                }
//
//                $data['id'] = $PARAM4;
//                $data['createdBy'] = $PARAM3;
//                $data['message'] = "New quiz has been created: " . $PARAM2;
//                $push->setFlag(PUSH_FLAG_CREATE_QUIZ);
//                break;

            case PUSH_FLAG_CREATE_GRADE:
                $users = $this->getUsersJoinedForFostan($PARAM1);
                $registration_ids = array();
                // preparing gcm registration ids array

                if ($users != null) {
                    foreach ($users as $u) {
                        array_push($registration_ids, $u['registration_id']);
                    }
                } else {
                    $registration_ids = null;
                }

                $data['id'] = $PARAM1;
                $data['student_id'] = $PARAM3;
                $data['createdBy'] = null;
                $data['message'] = "New Grade has been created for " . $PARAM2 . " fostan";
                $push->setFlag(PUSH_FLAG_CREATE_GRADE);
                break;
        }


        $push->setTitle("Google Cloud Messaging");
        $push->setIsBackground(FALSE);
        $push->setData($data);

        // sending push message to multiple users
        if ($registration_ids != null) {
            $gcm->sendMultiple($registration_ids, $push->getPush());
        }
    }

    public function getUsersJoinedForFostanWithCreator($fostan_id) {

        $users = array();

        $stmt = $this->conn->prepare("select u.registration_id from users u 
                                            where u.user_id in (select distinct student_id from fostans_student where fostan_id = ?)
                                            union select u.registration_id from users u where u.USER_ID 
                                            in (select cr.CHAT_ROOM_CREATED_BY from chat_rooms cr where cr.FOSTAN_ID = ?);");
        $stmt->bind_param("ii", $fostan_id, $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($registration_id);
        if ($num_of_rows > 0) {
            while ($stmt->fetch()) {
                $temp = array();
                $temp['registration_id'] = $registration_id;
                array_push($users, $temp);
            }
            $stmt->close();
            return $users;
        }
        $stmt->close();
        return null;
    }

    public function getUsersJoinedForFostan($fostan_id) {

        $users = array();

        $stmt = $this->conn->prepare("select u.registration_id from users u where user_id in 
                                        (select distinct student_id from fostans_student where fostan_id = ?) ");
        $stmt->bind_param("i", $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($registration_id);
        if ($num_of_rows > 0) {
            while ($stmt->fetch()) {
                $temp = array();
                $temp['registration_id'] = $registration_id;
                array_push($users, $temp);
            }
            $stmt->close();
            return $users;
        }
        $stmt->close();
        return null;
    }

    public function getStudentForDept($dept_id) {

        $users = array();

        $stmt = $this->conn->prepare("select registration_id from users where USER_DEPT = ? ");
        $stmt->bind_param("i", $dept_id);
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($registration_id);
        if ($num_of_rows > 0) {
            while ($stmt->fetch()) {
                $temp = array();
                $temp['registration_id'] = $registration_id;
                array_push($users, $temp);
            }
            $stmt->close();
            return $users;
        }
        $stmt->close();
        return null;
    }

    public function getAllUsers() {

        $users = array();

        $stmt = $this->conn->prepare("select distinct registration_id from users ");
        $stmt->execute();
        $stmt->store_result();
        $num_of_rows = $stmt->num_rows;
        $stmt->bind_result($registration_id);
        if ($num_of_rows > 0) {
            while ($stmt->fetch()) {
                $temp = array();
                $temp['registration_id'] = $registration_id;
                array_push($users, $temp);
            }
            $stmt->close();
            return $users;
        }
        $stmt->close();
        return null;
    }

    public function commitDB() {
        $this->conn->commit();
    }

    public function rollbackDB() {
        $this->conn->rollback();
    }

}
