<?php

error_reporting(-1);
ini_set('display_errors', 'On');

require_once '../include/db_handler.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$app->response->headers->set('Content-Type', 'application/json');


/**
 * **************USERS TABLE ************
 * */
//User Login
$app->post('/user/login', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('user_name', 'user_password'));

    try {
        //reading post params
        $user_name = $app->request->post('user_name');
        $user_password = $app->request->post('user_password');

        $db = new DbHandler();
        $response = $db->login($user_name, $user_password);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//create user
$app->post('/user/create_user', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('USER_FULL_NAME', 'USER_NAME', 'USER_PASSWORD', 'USER_EMAIL', 'USER_GENDER', 'USER_RULE', 'USER_DEPT'));

    //reading post params
    try {
        $USER_FULL_NAME = $app->request->post('USER_FULL_NAME');
        $USER_NAME = $app->request->post('USER_NAME');
        $USER_PASSWORD = $app->request->post('USER_PASSWORD');
        $USER_EMAIL = $app->request->post('USER_EMAIL');
        $USER_GENDER = $app->request->post('USER_GENDER');
        $USER_PIC = $app->request->post('USER_PIC');
        $USER_MOBILE = $app->request->post('USER_MOBILE');
        $USER_RULE = $app->request->post('USER_RULE');
        $USER_DEPT = $app->request->post('USER_DEPT');

        $db = new DbHandler();
        $response = $db->createUser($USER_FULL_NAME, $USER_NAME, $USER_PASSWORD, $USER_EMAIL, $USER_GENDER, $USER_PIC, $USER_MOBILE, $USER_RULE, $USER_DEPT);
        $db->commitDB();
    } catch (Exception $ex) {
        $db->rollbackDB();

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }



    echoResponse(200, $response);
});

//Update User Info
$app->post('/user/update_user_info', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('USER_ID', 'USER_FULL_NAME', 'USER_PASSWORD', 'USER_MOBILE', 'USER_PIC', 'USER_EMAIL'));

    try {
        $USER_ID = $app->request->post('USER_ID');
        $USER_FULL_NAME = $app->request->post('USER_FULL_NAME');
        $USER_PASSWORD = $app->request->post('USER_PASSWORD');
        $USER_EMAIL = $app->request->post('USER_EMAIL');
        $USER_PIC = $app->request->post('USER_PIC');
        $USER_MOBILE = $app->request->post('USER_MOBILE');
//    $USER_DEPT = $app->request->post('USER_DEPT');

        $db = new DbHandler();
        $response = $db->updateUserInfo($USER_ID, $USER_FULL_NAME, $USER_EMAIL, $USER_PASSWORD, $USER_PIC, $USER_MOBILE);
    } catch (Exception $ex) {
        $db->rollbackDB();

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }


    echoResponse(200, $response);
});

//Update REG ID
$app->post('/user/update_regid', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('gcm_registration_id', 'user_id'));

    try {
        $gcm_registration_id = $app->request->post('gcm_registration_id');
        $user_id = $app->request->post('user_id');
        $db = new DbHandler();
        if ($db->updateRegID($user_id, $gcm_registration_id)) {
            $response["error"] = false;
            $response["message"] = "Reg ID Updated Successfully";
        } else {
            $response["error"] = true;
            $response["message"] = "Reg ID Not Updated ";
        }
    } catch (Exception $ex) {
        $db->rollbackDB();

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});



/**
 * **************FOSTANS TABLE ************
 * */
//create Fostan
$app->post('/user/create_fostan', function() use ($app) {


    verifyRequiredParams(array(
        'fostanSize',
        'fostanType',
        'fostanFor',
        'fostanPrice',
        'fostan_title',
        'fostanDescription',
        'user_id',
        'show_number',
        'fostanCity'));

    $fostanSize = $app->request->post('fostanSize');
    $fostanType = $app->request->post('fostanType');
    $fostanFor = $app->request->post('fostanFor');
    $fostanPrice = $app->request->post('fostanPrice');
    $fostanTitle = $app->request->post('fostan_title');
    $fostanDescription = $app->request->post('fostanDescription');
    $user_id = $app->request->post('user_id');
    $show_number = $app->request->post('show_number');
    $fostan_city = $app->request->post('fostanCity');
    $fostanImg1 = $app->request->post('fostanImg1');
    $fostanImg2 = $app->request->post('fostanImg2');
    $fostanImg3 = $app->request->post('fostanImg3');

    $db = new DbHandler();
    $response = array();
    try {
        $fostan_id = $db->createFostan($fostanSize, $fostanType, $fostanFor, $fostanPrice, $fostanTitle, $fostanDescription, $user_id, $show_number, $fostan_city, $fostanImg1, $fostanImg2, $fostanImg3);

        if ($fostan_id != null) {

//            if (!empty($_FILES['uploaded_file1']['name'])) {
//                $file_name1 = basename($_FILES['uploaded_file1']['name']);
//                $file_content_1 = $_FILES['uploaded_file1']['tmp_name'];
//
//                $ATTACHMENT_ID1 = $db->uploadAttachments($file_name1, $file_content_1);
//                if (!($ATTACHMENT_ID1 != null && $db->insertAttachmentForFostan($fostan_id, $ATTACHMENT_ID1) != null)) {
//                    $response["error"] = true;
//                    $response["message"] = "Attachment 1 not uploaded successfully";
//                    $db->rollbackDB();
//                    echoResponse(400, $response);
//                    $app->stop();
//                }
//            }
//
//            if (!empty($_FILES['uploaded_file2']['name'])) {
//                $file_name2 = basename($_FILES['uploaded_file2']['name']);
//                $file_content_2 = $_FILES['uploaded_file2']['tmp_name'];
//
//                $ATTACHMENT_ID2 = $db->uploadAttachments($file_name2, $file_content_2);
//                if (!($ATTACHMENT_ID2 != null && $db->insertAttachmentForFostan($fostan_id, $ATTACHMENT_ID2) != null)) {
//                    $response["error"] = true;
//                    $response["message"] = "Attachment 2 not uploaded successfully";
//                    $db->rollbackDB();
//                    echoResponse(400, $response);
//                    $app->stop();
//                }
//            }
//
//            if (!empty($_FILES['uploaded_file3']['name'])) {
//                $file_name3 = basename($_FILES['uploaded_file3']['name']);
//                $file_content_3 = $_FILES['uploaded_file3']['tmp_name'];
//
//                $ATTACHMENT_ID3 = $db->uploadAttachments($file_name3, $file_content_3);
//                if (!($ATTACHMENT_ID3 != null && $db->insertAttachmentForFostan($fostan_id, $ATTACHMENT_ID3) != null)) {
//                    $response["error"] = true;
//                    $response["message"] = "Attachment 3 not uploaded successfully";
//                    $db->rollbackDB();
//                    echoResponse(400, $response);
//                    $app->stop();
//                }
//            }

            $response["error"] = false;
            $response["message"] = "Fostan Has Been Created Successfully";

            // $db->sendNewCreationNotification(PUSH_FLAG_CREATE_FOSTANS, $DEPT_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $fostan_id);
            $db->commitDB();
        } else {
            $response["error"] = true;
            $response["message"] = "Fostan Not Created";
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Delete Fostan
$app->post('/instructor/delete_fostan', function() use ($app) {
    verifyRequiredParams(array('fostan_id'));

    try {
        $FOSTAN_ID = $app->request->post('fostan_id');

        $db = new DbHandler();
        $response = $db->deleteFostan($FOSTAN_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Update Fostan 
$app->post('/instructor/update_fostan', function() use ($app) {
    verifyRequiredParams(array('FOSTAN_ID', 'FOSTAN_NAME', 'INSTRUCTOR_ID', 'DEPT_ID', 'cs_id1', 'cs_id2', 'cs_id3'));

    $FOSTAN_ID = $app->request->post('FOSTAN_ID');
    $FOSTAN_NAME = $app->request->post('FOSTAN_NAME');
    $INSTRUCTOR_ID = $app->request->post('INSTRUCTOR_ID');
    $DEPT_ID = $app->request->post('DEPT_ID');
    $FOSTAN_DESC = $app->request->post('FOSTAN_DESC');

    $cs_id1 = $app->request->post('cs_id1');
    $cs_id2 = $app->request->post('cs_id2');
    $cs_id3 = $app->request->post('cs_id3');

    $DELETED_1 = $app->request()->post('DELETED_1');
    $DELETED_2 = $app->request()->post('DELETED_2');
    $DELETED_3 = $app->request()->post('DELETED_3');


    $db = new DbHandler();
    $response = array();

    try {
        if ($db->updateFostan($FOSTAN_ID, $FOSTAN_NAME, $FOSTAN_DESC) != null) {

            if (!empty($_FILES['uploaded_file1']['name'])) {
                $file_name1 = basename($_FILES['uploaded_file1']['name']);
                $file_content_1 = $_FILES['uploaded_file1']['tmp_name'];

                $ATTACHMENT_ID1 = $db->uploadAttachments($file_name1, $file_content_1);
                if ($ATTACHMENT_ID1 != null) {
                    if ($cs_id1 == "0") {
                        if (!($db->insertAttachmentForFostan($FOSTAN_ID, $ATTACHMENT_ID1) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 1 not uploaded successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    } else {
                        if (!($db->updateAttachmentForFostan($FOSTAN_ID, $ATTACHMENT_ID1, $cs_id1) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 1 not updated successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    }
                }
            }

            if (!empty($_FILES['uploaded_file2']['name'])) {
                $file_name2 = basename($_FILES['uploaded_file2']['name']);
                $file_content_2 = $_FILES['uploaded_file2']['tmp_name'];

                $ATTACHMENT_ID2 = $db->uploadAttachments($file_name2, $file_content_2);
                if ($ATTACHMENT_ID2 != null) {
                    if ($cs_id2 == "0") {
                        if (!($db->insertAttachmentForFostan($FOSTAN_ID, $ATTACHMENT_ID2) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 2 not uploaded successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    } else {
                        if (!($db->updateAttachmentForFostan($FOSTAN_ID, $ATTACHMENT_ID2, $cs_id2) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 2 not updated successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    }
                }
            }

            if (!empty($_FILES['uploaded_file3']['name'])) {
                $file_name3 = basename($_FILES['uploaded_file3']['name']);
                $file_content_3 = $_FILES['uploaded_file3']['tmp_name'];

                $ATTACHMENT_ID3 = $db->uploadAttachments($file_name3, $file_content_3);
                if ($ATTACHMENT_ID3 != null) {
                    if ($cs_id3 == "0") {
                        if (!($db->insertAttachmentForFostan($FOSTAN_ID, $ATTACHMENT_ID3) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 3 not updated successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    } else {
                        if (!($db->updateAttachmentForFostan($FOSTAN_ID, $ATTACHMENT_ID3, $cs_id3) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 3 not updated successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    }
                }
            }

            if (!empty($DELETED_1)) {
                if (!($db->removeAttachmentForFostan($cs_id1) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 1 not deleted successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }
            if (!empty($DELETED_2)) {
                if (!($db->removeAttachmentForFostan($cs_id2) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 2 not deleted successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }
            if (!empty($DELETED_3)) {
                if (!($db->removeAttachmentForFostan($cs_id3) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 3 not deleted successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }
            $response["error"] = false;
            $response["message"] = "Fostan Has Been Updated Successfully";
            $db->commitDB();
        } else {
            $response["error"] = true;
            $response["message"] = "Fostan Not Updated";
            $db->rollbackDB();
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Join Fostan
$app->post('/student/join_fostan', function() use ($app) {


    verifyRequiredParams(array('student_id', 'fostan_id'));

    try {
        $student_id = $app->request->post('student_id');
        $fostan_id = $app->request->post('fostan_id');

        $db = new DbHandler();
        $response = $db->joinFostan($student_id, $fostan_id);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Disjoin Fostan
$app->post('/user/disjoin_fostan', function() use ($app) {
    verifyRequiredParams(array('fostan_id', 'student_id'));

    try {
        $FOSTAN_ID = $app->request->post('fostan_id');
        $STUDENT_ID = $app->request->post('student_id');

        $db = new DbHandler();
        $response = $db->disJoinFostan($FOSTAN_ID, $STUDENT_ID);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//GET Joined Fostans Student
$app->post('/user/get_joined_fostans', function() use ($app) {
    verifyRequiredParams(array('student_id'));
    try {
        $STUDENT_ID = $app->request->post('student_id');
        $db = new DbHandler();
        $response = $db->getJoinedfostans($STUDENT_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//GET Submited Quizes Fostans
$app->post('/student/get_submitted_quizes_fostans', function() use ($app) {
    verifyRequiredParams(array('student_id'));
    try {
        $STUDENT_ID = $app->request->post('student_id');
        $db = new DbHandler();
        $response = $db->getSubmitedQuizesFostans($STUDENT_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get All Fostans For The Department
$app->post('/instructor/GetAllFostansDept', function() use ($app) {
    verifyRequiredParams(array('dept_id', 'start', 'end'));
    try {
        $DEPT_ID = $app->request->post('dept_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getAllFostansDept($start, $end, $DEPT_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get fostan by ID
$app->post('/instructor/getFostanByID', function() use ($app) {
    verifyRequiredParams(array('fostan_id'));
    try {
        $fostan_id = $app->request->post('fostan_id');
        $db = new DbHandler();
        $response = $db->getFostanByID($fostan_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get -** All **- fostans to the Student joined and not joined fostans with column named isJoined
$app->post('/student/GetAllFostansStudent', function() use ($app) {
    verifyRequiredParams(array('student_id', 'dept_id', 'start', 'end'));

    try {
        $student_id = $app->request->post('student_id');
        $dept_id = $app->request->post('dept_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getAllFostansStudent($start, $end, $student_id, $dept_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get All Users For The Fostan
$app->post('/instructor/GetStudentForFostan', function() use ($app) {
    verifyRequiredParams(array('fostan_id'));

    try {
        $FOSTAN_ID = $app->request->post('fostan_id');

        $db = new DbHandler();
        if (!$db->fostanGradesSubmitted($FOSTAN_ID)) {
            $response = $db->getAllUsersForFostan($FOSTAN_ID);
        } else {
            $response['error'] = true;
            $response['message'] = "You Submitted Grades For This Fostan Before";
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});



//Get fostans to the Student only joined fostans
//$app->post('/student/GetFostansStudent', function() use ($app) {
//    verifyRequiredParams(array('student_id', 'start', 'end'));
//
//    try {
//        $student_id = $app->request->post('student_id');
//        $start = $app->request->post('start');
//        $end = $app->request->post('end');
//
//        $db = new DbHandler();
//        $result = $db->getFostansStudent($start, $end, $student_id);
//        if ($result != null) {
//            $response = array();
//            $response['error'] = false;
//            $response['data'] = array();
//
//            while ($row = $result->fetch_assoc()) {
//                $temp = array();
//                $temp['FOSTAN_ID'] = $row['FOSTAN_ID'];
//                $temp['FOSTAN_NAME'] = $row['FOSTAN_NAME'];
//                $temp['DEPT_ID'] = $row['DEPT_ID'];
//                $temp['INSTRUCTOR_ID'] = $row['INSTRUCTOR_ID'];
//                $temp['INSTRUCTOR_NAME'] = $row['USER_FULL_NAME'];
//                $temp['FOSTAN_DESC'] = $row['FOSTAN_DESC'];
//                $temp['FOSTAN_CREATED_AT'] = $row['FOSTAN_CREATED_AT'];
//                array_push($response['data'], $temp);
//            }
//        } else {
//            $response['error'] = true;
//            $response['message'] = "No Data Found";
//        }
//    } catch (Exception $ex) {
//        $response["error"] = true;
//        $response["message"] = $ex->getMessage();
//    }
//    echoResponse(200, $response);
//});
//Get All Fostans For The Instructor
$app->post('/instructor/GetAllFostansInstructor', function() use ($app) {
    verifyRequiredParams(array('instructor_id', 'start', 'end'));
    try {
        $instructor_id = $app->request->post('instructor_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();

        $response = $db->getAllFostansInstructor($start, $end, $instructor_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});


//Get attachments For The Fostan
$app->post('/user/GetAttachmentsForFostan', function() use ($app) {
    verifyRequiredParams(array('fostan_id'));

    $fostan_id = $app->request->post('fostan_id');
    $db = new DbHandler();

    try {
        $response = $db->getAttachmentsForFostan($fostan_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});
/**
 * **************ANNOUNCEMENTS TABLE ************
 * */
//Create ANNOUNCEMENT
$app->post('/instructor/create_announcement', function() use ($app) {

    verifyRequiredParams(array('USER_ID'));

    try {
        $ANNOUNCEMENT_DESC = $app->request->post('ANNOUNCEMENT_DESC');
        $USER_ID = $app->request->post('USER_ID');
        $ANNOUNCEMENT_PIC = $app->request->post('IMAGE');

        $db = new DbHandler();
        $response = $db->createAnnouncement($ANNOUNCEMENT_DESC, $ANNOUNCEMENT_PIC, $USER_ID);
        // $db->sendNewCreationNotification(PUSH_FLAG_CREATE_ANNOUNCEMENT, null, $ANNOUNCEMENT_DESC, $USER_ID, $response["id"]);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});

//Delete ANNOUNCEMENT
$app->post('/instructor/delete_announcement', function() use ($app) {
    verifyRequiredParams(array('announcement_id'));

    try {
        $ANNOUNCEMENT_ID = $app->request->post('announcement_id');

        $db = new DbHandler();
        $response = $db->deleteAnnouncement($ANNOUNCEMENT_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Update Announcements 
$app->post('/instructor/update_announcement', function() use ($app) {
    verifyRequiredParams(array('ANNOUNCEMENT_ID'));
    try {
        $ANNOUNCEMENT_DESC = $app->request->post('ANNOUNCEMENT_DESC');
        $ANNOUNCEMENT_PIC = $app->request->post('IMAGE');
        $ANNOUNCEMENT_ID = $app->request->post('ANNOUNCEMENT_ID');

        $db = new DbHandler();
        $response = $db->updateAnnouncement($ANNOUNCEMENT_ID, $ANNOUNCEMENT_DESC, $ANNOUNCEMENT_PIC);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get All Fostans For The Announcements
$app->post('/instructor/GetAllAnnouncements', function() use ($app) {
    try {
        verifyRequiredParams(array('start', 'end'));
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getAllAnnouncements($start, $end);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});

//Get Announcement By ID
$app->post('/user/getAnnouncementByID', function() use ($app) {
    try {
        verifyRequiredParams(array('announcement_id'));
        $announcement_id = $app->request->post('announcement_id');

        $db = new DbHandler();
        $response = $db->getAnnouncementByID($announcement_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});

//Get All Fostans For The Announcements FOR Instructor
$app->post('/instructor/GetAllAnnouncementsForInstructor', function() use ($app) {
    verifyRequiredParams(array('start', 'end', 'user_id'));

    try {
        $start = $app->request->post('start');
        $end = $app->request->post('end');
        $user_id = $app->request->post('user_id');

        $db = new DbHandler();
        $response = $db->getAllAnnouncementsForInstructor($start, $end, $user_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

/**
 * **************ASSIGNEMNTS TABLE ************
 * */
//Create ASSIGNMENT
$app->post('/instructor/create_assignment', function() use ($app) {

    verifyRequiredParams(array('ASSIGNMENT_TITLE', 'ASSIGNMENT_DESC', 'ASSIGNMENT_DEADLINE', 'FOSTAN_ID', 'USER_ID'));

    $ASSIGNMENT_TITLE = $app->request->post('ASSIGNMENT_TITLE');
    $ASSIGNMENT_DESC = $app->request->post('ASSIGNMENT_DESC');
    $ASSIGNMENT_DEADLINE = $app->request->post('ASSIGNMENT_DEADLINE');
    $FOSTAN_ID = $app->request->post('FOSTAN_ID');
    $USER_ID = $app->request->post('USER_ID');

    $db = new DbHandler();
    $response = array();
    try {
        $assignment_id = $db->createAssignment($ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE, $FOSTAN_ID, $USER_ID);
        if ($assignment_id != null) {

            if (!empty($_FILES['uploaded_file1']['name'])) {
                $file_name1 = basename($_FILES['uploaded_file1']['name']);
                $file_content_1 = $_FILES['uploaded_file1']['tmp_name'];

                $ATTACHMENT_ID1 = $db->uploadAttachments($file_name1, $file_content_1);
                if (!($ATTACHMENT_ID1 != null && $db->insertAttachmentForAssignment($assignment_id, $ATTACHMENT_ID1) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 1 not uploaded successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }

            if (!empty($_FILES['uploaded_file2']['name'])) {
                $file_name2 = basename($_FILES['uploaded_file2']['name']);
                $file_content_2 = $_FILES['uploaded_file2']['tmp_name'];

                $ATTACHMENT_ID2 = $db->uploadAttachments($file_name2, $file_content_2);
                if (!($ATTACHMENT_ID2 != null && $db->insertAttachmentForAssignment($assignment_id, $ATTACHMENT_ID2) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 2 not uploaded successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }

            if (!empty($_FILES['uploaded_file3']['name'])) {
                $file_name3 = basename($_FILES['uploaded_file3']['name']);
                $file_content_3 = $_FILES['uploaded_file3']['tmp_name'];

                $ATTACHMENT_ID3 = $db->uploadAttachments($file_name3, $file_content_3);
                if (!($ATTACHMENT_ID3 != null && $db->insertAttachmentForAssignment($assignment_id, $ATTACHMENT_ID3) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 3 not uploaded successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }

            $response["error"] = false;
            $response["message"] = "Assignment Has Been Created Successfully";

            // $db->sendNewCreationNotification(PUSH_FLAG_CREATE_ASSIGNMENT, $FOSTAN_ID, $ASSIGNMENT_TITLE, $USER_ID, $assignment_id);
            $db->commitDB();
        } else {
            $response["error"] = true;
            $response["message"] = "Assignment Not Created";
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Delete ASSIGNMENT
$app->post('/instructor/delete_assignment', function() use ($app) {
    verifyRequiredParams(array('assignment_id'));

    try {
        $ASSIGNMENT_ID = $app->request->post('assignment_id');

        $db = new DbHandler();
        $response = $db->deleteAssignment($ASSIGNMENT_ID);
    } catch (Exception $ex) {
        $response['error'] = true;
        $response['message'] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Update ASSIGNMENT 
$app->post('/instructor/update_assignment', function() use ($app) {
    verifyRequiredParams(array('ASSIGNMENT_ID', 'ASSIGNMENT_TITLE', 'ASSIGNMENT_DESC', 'ASSIGNMENT_DEADLINE', 'aa_id1', 'aa_id2', 'aa_id3'));

    $ASSIGNMENT_ID = $app->request->post('ASSIGNMENT_ID');
    $ASSIGNMENT_TITLE = $app->request->post('ASSIGNMENT_TITLE');
    $ASSIGNMENT_DESC = $app->request->post('ASSIGNMENT_DESC');
    $ASSIGNMENT_DEADLINE = $app->request->post('ASSIGNMENT_DEADLINE');
    $ATTACHMENT = $app->request->post('ATTACHMENT');

    $aa_id1 = $app->request->post('aa_id1');
    $aa_id2 = $app->request->post('aa_id2');
    $aa_id3 = $app->request->post('aa_id3');

    $DELETED_1 = $app->request()->post('DELETED_1');
    $DELETED_2 = $app->request()->post('DELETED_2');
    $DELETED_3 = $app->request()->post('DELETED_3');

    $db = new DbHandler();
    $response = array();
    try {
        if ($db->updateAssignment($ASSIGNMENT_ID, $ASSIGNMENT_TITLE, $ASSIGNMENT_DESC, $ASSIGNMENT_DEADLINE) != null) {

            if (!empty($_FILES['uploaded_file1']['name'])) {
                $file_name1 = basename($_FILES['uploaded_file1']['name']);
                $file_content_1 = $_FILES['uploaded_file1']['tmp_name'];

                $ATTACHMENT_ID1 = $db->uploadAttachments($file_name1, $file_content_1);
                if ($ATTACHMENT_ID1 != null) {
                    if ($aa_id1 == "0") {
                        if (!($db->insertAttachmentForAssignment($ASSIGNMENT_ID, $ATTACHMENT_ID1) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 1 not uploaded successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    } else {
                        if (!($db->updateAttachmentForAssignment($ASSIGNMENT_ID, $ATTACHMENT_ID1, $aa_id1) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 1 not updated successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    }
                }
            }

            if (!empty($_FILES['uploaded_file2']['name'])) {
                $file_name2 = basename($_FILES['uploaded_file2']['name']);
                $file_content_2 = $_FILES['uploaded_file2']['tmp_name'];

                $ATTACHMENT_ID2 = $db->uploadAttachments($file_name2, $file_content_2);
                if ($ATTACHMENT_ID2 != null) {
                    if ($aa_id2 == "0") {
                        if (!($db->insertAttachmentForAssignment($ASSIGNMENT_ID, $ATTACHMENT_ID2) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 2 not uploaded successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    } else {
                        if (!($db->updateAttachmentForAssignment($ASSIGNMENT_ID, $ATTACHMENT_ID2, $aa_id2) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 2 not updated successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    }
                }
            }

            if (!empty($_FILES['uploaded_file3']['name'])) {
                $file_name3 = basename($_FILES['uploaded_file3']['name']);
                $file_content_3 = $_FILES['uploaded_file3']['tmp_name'];

                $ATTACHMENT_ID3 = $db->uploadAttachments($file_name3, $file_content_3);
                if ($ATTACHMENT_ID3 != null) {
                    if ($aa_id3 == "0") {
                        if (!($db->insertAttachmentForAssignment($ASSIGNMENT_ID, $ATTACHMENT_ID3) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 3 not updated successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    } else {
                        if (!($db->updateAttachmentForAssignment($ASSIGNMENT_ID, $ATTACHMENT_ID3, $aa_id3) != null)) {
                            $response["error"] = true;
                            $response["message"] = "Attachment 3 not updated successfully";
                            $db->rollbackDB();
                            echoResponse(400, $response);
                            $app->stop();
                        }
                    }
                }
            }

            if (!empty($DELETED_1)) {
                if (!($db->removeAttachmentForAssignment($aa_id1) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 1 not deleted successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }
            if (!empty($DELETED_2)) {
                if (!($db->removeAttachmentForAssignment($aa_id2) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 2 not deleted successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }
            if (!empty($DELETED_3)) {
                if (!($db->removeAttachmentForAssignment($aa_id3) != null)) {
                    $response["error"] = true;
                    $response["message"] = "Attachment 3 not deleted successfully";
                    $db->rollbackDB();
                    echoResponse(400, $response);
                    $app->stop();
                }
            }

            $response["error"] = false;
            $response["message"] = "Assignment Has Been Updated Successfully";
            $db->commitDB();
        } else {
            $response["error"] = true;
            $response["message"] = "Assignment Not Updated";
            $db->rollbackDB();
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});

//Get Assignemnts For Instructor
$app->post('/instructor/GetInstructorAssignemnts', function() use ($app) {
    verifyRequiredParams(array('fostan_id', 'instructor_id', 'start', 'end'));

    try {
        $fostan_id = $app->request->post('fostan_id');
        $instructor_id = $app->request->post('instructor_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getInstructorAssignments($start, $end, $fostan_id, $instructor_id);
    } catch (Exception $ex) {
        $response['error'] = true;
        $response['message'] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Assignemnts For Student
$app->post('/user/GetStudentAssignemnts', function() use ($app) {
    verifyRequiredParams(array('fostan_id', 'student_id', 'start', 'end'));

    try {
        $fostan_id = $app->request->post('fostan_id');
        $student_id = $app->request->post('student_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getStudentAssignments($start, $end, $fostan_id, $student_id);
    } catch (Exception $ex) {
        $response['error'] = true;
        $response['message'] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Assignemnts For Student
$app->post('/user/getAssignmentByID', function() use ($app) {
    verifyRequiredParams(array('assignment_id'));

    try {
        $assignment_id = $app->request->post('assignment_id');

        $db = new DbHandler();
        $response = $db->getAssignmentByID($assignment_id);
    } catch (Exception $ex) {
        $response['error'] = true;
        $response['message'] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get attachments For The Assignment
$app->post('/user/GetAttachmentsForAssignment', function() use ($app) {
    verifyRequiredParams(array('assignment_id'));

    $assignment_id = $app->request->post('assignment_id');
    $db = new DbHandler();

    try {
        $response = $db->getAttachmentsForAssignments($assignment_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});


/**
 * ********************** Quiz Table *******************************
 */
//Create Quiz Batch
$app->post('/instructor/create_quiz', function() use ($app) {

    verifyRequiredParams(array('QUIZ_NAME', 'USER_ID', 'FOSTAN_ID'));

    $QUIZ_NAME = $app->request->post('QUIZ_NAME');
    $USER_ID = $app->request->post('USER_ID');
    $FOSTAN_ID = $app->request->post('FOSTAN_ID');

    $db = new DbHandler();
    $response = $db->createQuiz($QUIZ_NAME, $USER_ID, $FOSTAN_ID);

    echoResponse(200, $response);
});

//Delete Quiz
$app->post('/instructor/delete_quiz', function() use ($app) {
    verifyRequiredParams(array('quiz_id'));

    $QUIZ_ID = $app->request->post('quiz_id');

    $db = new DbHandler();
    $response = $db->deleteQuiz($QUIZ_ID);

    echoResponse(200, $response);
});

//Update Quiz 
$app->post('/instructor/update_quiz', function() use ($app) {
    verifyRequiredParams(array('QUIZ_ID', 'QUIZ_NAME', 'FOSTAN_ID'));

    $QUIZ_ID = $app->request->post('QUIZ_ID');
    $FOSTAN_ID = $app->request->post('FOSTAN_ID');
    $QUIZ_NAME = $app->request->post('QUIZ_NAME');
    $db = new DbHandler();
    $response = $db->updateQuiz($QUIZ_ID, $QUIZ_NAME, $FOSTAN_ID);

    echoResponse(200, $response);
});

//Get All Quizs For Student
$app->post('/instructor/GetAllQuizsForStudent', function() use ($app) {
    verifyRequiredParams(array('fostan_id', 'user_id', 'start', 'end'));


    try {
        $fostan_id = $app->request->post('fostan_id');
        $user_id = $app->request->post('user_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getAllQuizsForStudent($start, $end, $fostan_id, $user_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get All Quizs For the fostan
$app->post('/instructor/GetAllQuizsForFostan', function() use ($app) {
    verifyRequiredParams(array('fostan_id'));


    try {
        $fostan_id = $app->request->post('fostan_id');

        $db = new DbHandler();
        $response = $db->getAllQuizsForFostan($fostan_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get All Quizs For Instructor
$app->post('/instructor/GetAllQuizsForInstructor', function() use ($app) {
    verifyRequiredParams(array('fostan_id', 'user_id', 'start', 'end'));
    try {
        $fostan_id = $app->request->post('fostan_id');
        $user_id = $app->request->post('user_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getAllQuizsForInstructor($start, $end, $fostan_id, $user_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});


/**
 * *********************** Q Questions Table ***********************
 */
//Create QQ
$app->post('/instructor/create_QQ', function() use ($app) {

    verifyRequiredParams(array('QQ_TEXT', 'QQ_A1', 'QQ_A2', 'QQ_A3', 'QQ_A4', 'QQ_RA', 'QUIZ_ID'));

    try {
        $QQ_TEXT = $app->request->post('QQ_TEXT');
        $QQ_A1 = $app->request->post('QQ_A1');
        $QQ_A2 = $app->request->post('QQ_A2');
        $QQ_A3 = $app->request->post('QQ_A3');
        $QQ_A4 = $app->request->post('QQ_A4');
        $QQ_RA = $app->request->post('QQ_RA');
        $QUIZ_ID = $app->request->post('QUIZ_ID');

        $db = new DbHandler();
        $response = $db->createQQ($QQ_TEXT, $QQ_A1, $QQ_A2, $QQ_A3, $QQ_A4, $QQ_RA, $QUIZ_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});

//Delete QQ
$app->post('/instructor/delete_QQ', function() use ($app) {
    verifyRequiredParams(array('QQ_ID'));

    try {
        $QQ_ID = $app->request->post('QQ_ID');

        $db = new DbHandler();
        $response = $db->deleteQQ($QQ_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});

//Update Quiz 
$app->post('/instructor/update_QQ', function() use ($app) {
    verifyRequiredParams(array('QQ_TEXT', 'QQ_A1', 'QQ_A2', 'QQ_A3', 'QQ_A4', 'QQ_RA', 'QQ_ID'));
    try {
        $QQ_TEXT = $app->request->post('QQ_TEXT');
        $QQ_A1 = $app->request->post('QQ_A1');
        $QQ_A2 = $app->request->post('QQ_A2');
        $QQ_A3 = $app->request->post('QQ_A3');
        $QQ_A4 = $app->request->post('QQ_A4');
        $QQ_RA = $app->request->post('QQ_RA');
        $QQ_ID = $app->request->post('QQ_ID');

        $db = new DbHandler();
        $response = $db->updateQQ($QQ_ID, $QQ_TEXT, $QQ_A1, $QQ_A2, $QQ_A3, $QQ_A4, $QQ_RA);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get All Quizs For Instructor
$app->post('/instructor/GetAllQQForInstructor', function() use ($app) {
    verifyRequiredParams(array('quiz_id', 'start', 'end'));


    $quiz_id = $app->request->post('quiz_id');
    $start = $app->request->post('start');
    $end = $app->request->post('end');

    $db = new DbHandler();
    $result = $db->getAllQuizsForInstructor($start, $end, $quiz_id);
    if ($result != null) {
        $response = array();
        $response['error'] = false;
        $response['data'] = array();

        while ($row = $result->fetch_assoc()) {
            $temp = array();
            $temp['QQ_ID'] = $row['QQ_ID'];
            $temp['QQ_TEXT'] = $row['QQ_TEXT'];
            $temp['QQ_A1'] = $row['QQ_A1'];
            $temp['QQ_A2'] = $row['QQ_A2'];
            $temp['QQ_A3'] = $row['QQ_A3'];
            $temp['QQ_A4'] = $row['QQ_A4'];
            $temp['QQ_RA'] = $row['QQ_RA'];

            array_push($response['data'], $temp);
        }
    } else {
        $response['error'] = true;
        $response['error'] = "Error while getting quiz questions";
    }
    echoResponse(200, $response);
});


//Get QQ Student
$app->post('/instructor/GetQQ', function() use ($app) {
    verifyRequiredParams(array('quiz_id'));
    try {
        $quiz_id = $app->request->post('quiz_id');


        $db = new DbHandler();
        $response = $db->getQQStudent($quiz_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get QQ Instructor
$app->post('/instructor/GetQQInstructor', function() use ($app) {
    verifyRequiredParams(array('quiz_id', 'start', 'end'));
    try {
        $quiz_id = $app->request->post('quiz_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getQQInstructor($start, $end, $quiz_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

/**
 * ******************* Q Results Table ***********************
 */
//Create QR
$app->post('/instructor/create_QR', function() use ($app) {

    verifyRequiredParams(array('QR_MARK', 'QUIZ_ID', 'STUDENT_ID'));

    try {
        $QR_MARK = $app->request->post('QR_MARK');
        $QUIZ_ID = $app->request->post('QUIZ_ID');
        $STUDENT_ID = $app->request->post('STUDENT_ID');

        $db = new DbHandler();
        $response = $db->createQuizResult($QR_MARK, $QUIZ_ID, $STUDENT_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get All Quiz Results for Instructor 
//$app->post('/instructor/GetAllQRForInstructor', function() use ($app) {
//    verifyRequiredParams(array('fostan_id', 'start', 'end'));
//    $response = array();
//    try {
//        $fostan_id = $app->request->post('fostan_id');
//        $start = $app->request->post('start');
//        $end = $app->request->post('end');
//
//        $db = new DbHandler();
//        $response = $db->getAllQuizResultsForInstructor($start, $end, $fostan_id);
//    } catch (Exception $ex) {
//        $response["error"] = true;
//        $response["message"] = $ex->getMessage();
//    }
//    echoResponse(200, $response);
//});
//Get Quiz Result for Student 
$app->post('/student/GetQRForStudent', function() use ($app) {
    verifyRequiredParams(array('quiz_id', 'student_id'));

    try {
        $quiz_id = $app->request->post('quiz_id');
        $student_id = $app->request->post('student_id');

        $db = new DbHandler();
        $response = $db->getQuizResultsForStudent($quiz_id, $student_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});



/**
 * ******************* Grades Table ***********************
 */
//Create QR
$app->post('/instructor/create_grades', function() use ($app) {

    verifyRequiredParams(array('MARKS'));

    $JSON_MARKS = $app->request->post('MARKS');

    $db = new DbHandler();
    $response = $db->createGrades($JSON_MARKS);

    echoResponse(200, $response);
});

//Update Grades
$app->post('/instructor/update_grades', function() use ($app) {
    verifyRequiredParams(array('GRADE_MARK', 'FOSTAN_ID', 'STUDENT_ID'));

    $GRADE_MARK = $app->request->post('GRADE_MARK');
    $FOSTAN_ID = $app->request->post('FOSTAN_ID');
    $STUDENT_ID = $app->request->post('STUDENT_ID');

    $db = new DbHandler();
    $response = $db->updateGrades($GRADE_MARK, $FOSTAN_ID, $STUDENT_ID);

    echoResponse(200, $response);
});

//isGradesFilled
$app->post('/instructor/isGradesFilled', function() use ($app) {
    verifyRequiredParams(array('FOSTAN_ID'));
    $response = array();

    $fostan_id = $app->request->post('FOSTAN_ID');

    $db = new DbHandler();
    $result = $db->getAllQuizResultsForInstructor($fostan_id);
    if ($result == true) {
        $response['filled'] = true;
    } else {
        $response['filled'] = false;
    }
    echoResponse(200, $response);
});




//Get Grade Result For Student
$app->post('/student/GetStudentGrade', function() use ($app) {
    verifyRequiredParams(array('student_id', 'fostan_id'));

    try {
        $fostan_id = $app->request->post('fostan_id');
        $student_id = $app->request->post('student_id');

        $db = new DbHandler();
        $response = $db->getStudentGrade($fostan_id, $student_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

/* *
 *  ************************ CHAT Discussion********************************* 
 */

//Get Conversation For User
$app->post('/user/GetConversationForUser', function() use ($app) {
    verifyRequiredParams(array('user_id'));
    try {
        $user_id = $app->request->post('user_id');
        $db = new DbHandler();
        $response = $db->getConversationForUser($user_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get conversation From User
$app->post('/user/GetConversationFromUser', function() use ($app) {
    verifyRequiredParams(array('user_from', 'user_to', 'startItem', 'endItem'));
    try {
        $user_from = $app->request->post('user_from');
        $user_to = $app->request->post('user_to');
        $startItem = $app->request->post('startItem');
        $endItem = $app->request->post('endItem');

        $db = new DbHandler();
        $response = $db->getConversationFromUser($user_from, $user_to, $startItem, $endItem);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Send Message To User Descussion
$app->post('/users/:id/message', function($to_user_id) {
    global $app;
    $db = new DbHandler();

    verifyRequiredParams(array('message'));

    $from_user_id = $app->request->post('user_id');
    $message = $app->request->post('message');

    $response = $db->addMessage($from_user_id, $to_user_id, $message);

    if (!$response['error']) {
        require_once __DIR__ . '/../libs/gcm/gcm.php';
        require_once __DIR__ . '/../libs/gcm/push.php';
        $gcm = new GCM();
        $push = new Push();

        $user = $db->getUser($to_user_id);

        $data = array();
        $data['user'] = $user;
        $data['message'] = $response['message'];
        $data['image'] = '';

        $push->setTitle("Google Cloud Messaging");
        $push->setIsBackground(FALSE);
        $push->setFlag(PUSH_FLAG_USER);
        $push->setData($data);

        // sending push message to single user
        $gcm->send($user['registration_id'], $push->getPush());

        $response['error'] = false;
        $response['user'] = $user;
        $db->commitDB();
    } else {
        $db->rollbackDB();
    }

    echoResponse(200, $response);
});

//Get Instructors For Student
$app->post('/user/getInstructorsForStudent', function() use ($app) {
    verifyRequiredParams(array('user_id'));
    try {
        $user_id = $app->request->post('user_id');
        $db = new DbHandler();
        $response = $db->getInstructorsForStudent($user_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Students FOR Instructors
$app->post('/user/getStudentsForInstructor', function() use ($app) {
    verifyRequiredParams(array('user_id', 'fostan_id'));
    try {
        $user_id = $app->request->post('user_id');
        $fostan_id = $app->request->post('fostan_id');

        $db = new DbHandler();
        $response = $db->getStudentsForInstructor($user_id, $fostan_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});


/*
 *  *************************** Collaboration ******************
 */

/* *
 *  ************************ Collaboration ********************************* 
 */
$app->post('/user/getChatRoomsForFostan', function() use ($app) {
    verifyRequiredParams(array('fostan_id'));
    try {
        $fostan_id = $app->request->post('fostan_id');
        $db = new DbHandler();
        $response = $db->getChatRoomsForFostan($fostan_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Add Chat Room
$app->post('/user/addChatRoom', function() use ($app) {
    global $app;
    $db = new DbHandler();

    verifyRequiredParams(array('CHAT_ROOM_QUESTION', 'FOSTAN_ID', 'CHAT_ROOM_CREATED_BY'));

    $CHAT_ROOM_QUESTION = $app->request->post('CHAT_ROOM_QUESTION');
    $FOSTAN_ID = $app->request->post('FOSTAN_ID');
    $CHAT_ROOM_CREATED_BY = $app->request->post('CHAT_ROOM_CREATED_BY');

    $response = $db->addChatRoom($CHAT_ROOM_QUESTION, $FOSTAN_ID, $CHAT_ROOM_CREATED_BY);
    if (!$response['error']) {
        // $db->sendNewCreationNotification(PUSH_FLAG_CREATE_CHATROOM, $FOSTAN_ID, $CHAT_ROOM_QUESTION, $CHAT_ROOM_CREATED_BY, $response['id']);
        $db->commitDB();
    } else {
        $db->rollbackDB();
    }

    echoResponse(200, $response);
});

//Get Conversation for chat room
$app->post('/user/getConversationForChatRoom', function() use ($app) {
    verifyRequiredParams(array('chat_room_id', 'start', 'end'));
    try {
        $chat_room_id = $app->request->post('chat_room_id');
        $start = $app->request->post('start');
        $end = $app->request->post('end');

        $db = new DbHandler();
        $response = $db->getConversationForChatRoom($chat_room_id, $start, $end);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Send Message To User Collaboration
$app->post('/user/:chat_room_id/messageChatRoom', function($chat_room_id) {
    global $app;
    $db = new DbHandler();

    verifyRequiredParams(array('message'));

    $from_user_id = $app->request->post('user_id');
    $message = $app->request->post('message');

    $response = $db->addMessageCollaboration($from_user_id, $chat_room_id, $message);

    if (!$response['error']) {
        require_once __DIR__ . '/../libs/gcm/gcm.php';
        require_once __DIR__ . '/../libs/gcm/push.php';
        $gcm = new GCM();
        $push = new Push();

        $users = $db->getUsersForChatRoom($chat_room_id);
        $registration_ids = array();
        // preparing gcm registration ids array
        foreach ($users as $u) {
            array_push($registration_ids, $u['registration_id']);
        }


        $data = array();
        $data['message'] = $response['message'];
        $data['image'] = '';

        $push->setTitle("Google Cloud Messaging");
        $push->setIsBackground(FALSE);
        $push->setFlag(PUSH_FLAG_CHATROOM);
        $push->setData($data);

        // sending push message to multiple users
        $gcm->sendMultiple($registration_ids, $push->getPush());

        $response['error'] = false;
        $db->commitDB();
    } else {
        $db->rollbackDB();
    }

    echoResponse(200, $response);
});

/** * *************************** Helper Functions
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
    // setting response content type to json
    $app->contentType('application/json');
    echo json_encode($response);
}

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoResponse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

$app->run();

