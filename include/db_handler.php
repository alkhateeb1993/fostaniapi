<?php

class DbHandler {

    public $conn;
    public $sub_hours = '1';

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        //opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    //create new user if not exist
    public function createUser($user_name, $user_password, $user_email, $user_mobile, $user_country_id, $user_gender, $user_regid, $user_pic) {

        $response = array();

        //Check if the user is not exist in DB
        if (!$this->isEmailExist($user_email)) {
            if (!$this->isUsernameExists($user_name)) {
                if (!empty($user_pic)) {
                    $filePath = $this->DecodeImage($user_pic);
                    if ($filePath != null) {

                        $stmt = $this->conn->prepare(
                                "INSERT INTO users (user_name, user_password,user_email,user_mobile,user_image,user_country_id,"
                                . "user_gender,gcm_registration_id) VALUES(?,?,?,?,?,?,?,?)");
                        $stmt->bind_param("sssssiss", $user_name, $user_password, $user_email, $user_mobile, $filePath, $user_country_id, $user_gender, $user_regid);

                        $result = $stmt->execute();
                        $stmt->close();

                        if ($result) {
                            //User Successfully inserted
                            $response["error"] = false;
                            $response['message_en'] = "Registerd Successfully";
                            $response["message_ar"] = "تم التسجيل بنجاح";
                            $response['user'] = array();
                            array_push($response['user'], $this->getUserByEmail($user_email));
                        } else {
                            //Failed to create user
                            $response["error"] = true;
                            $response['message_en'] = "An error occured while registering";
                            $response["message_ar"] = "حصلت مشكله اثناء التسجيل";
                        }
                    } else {
                        $response["error"] = true;
                        $response['message_en'] = "Error While Uploading The Image";
                        $response["message_ar"] = "حصلت مشكله اثناء رفع الصوره";
                    }
                } else {
                    $stmt = $this->conn->prepare(
                            "INSERT INTO users (user_name, user_password,user_email,user_mobile,user_country_id,"
                            . "user_gender,gcm_registration_id) VALUES(?,?,?,?,?,?,?,?)");
                    $stmt->bind_param("sssssiss", $user_name, $user_password, $user_email, $user_mobile, $user_country_id, $user_gender, $user_regid);

                    $result = $stmt->execute();
                    $stmt->close();

                    if ($result) {
                        //User Successfully inserted
                        $response["error"] = false;
                        $response['message_en'] = "Registerd Successfully";
                        $response["message_ar"] = "تم التسجيل بنجاح";
                        $response['user'] = array();
                        array_push($response['user'], $this->getUserByEmail($user_email));
                    } else {
                        //Failed to create user
                        $response["error"] = true;
                        $response['message_en'] = "An error occured while registering";
                        $response["message_ar"] = "حصلت مشكله اثناء التسجيل";
                    }
                }
            } else {
                $response["error"] = true;
                $response['message_en'] = "This username is already registerd";
                $response["message_ar"] = "اسم المستخدم موجود مسبقا";
            }
        } else {
            $response["error"] = true;
            $response['message_en'] = "User with same email already exists";
            $response["message_ar"] = "البريد الالكتروني معرف مسبقا";
        }
        return $response;
    }

    //Login
    public function login($user_name, $user_password) {
        $response = array();

        $stmt = $this->conn->prepare("select * from users where user_email = ? and user_password = ?");
        $stmt->bind_param("ss", $user_name, $user_password);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        if ($num_rows > 0) {
            $stmt->bind_result($user_id, $user_name, $user_email, $user_password, $user_mobile, $user_image, $user_country_id, $user_gender, $user_created_at, $user_active, $gcm_registration_id);
            $stmt->fetch();
            $userData = array();

            $userData["user_id"] = $user_id;
            $userData["user_name"] = $user_name;
            $userData["user_password"] = $user_password;
            $userData["user_email"] = $user_email;
            $userData["user_mobile"] = $user_mobile;
            $userData["user_image"] = $user_image;
            $userData["user_country_id"] = $user_country_id;
            $userData["user_gender"] = $user_gender;
            $userData["user_created_at"] = $user_created_at;
            $userData["user_active"] = $user_active;
            $userData["user_regid"] = $gcm_registration_id;


            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $userData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "Username Or Password Is Incorrect";
            $response["message_ar"] = "اسم المستخدم او كلمة المرور غير صحيح";

            return $response;
        }
    }

    public function updateRegID($user_id, $reg_id) {
        $response = array();
        $stmt = $this->conn->prepare("UPDATE users SET gcm_registration_id = ? "
                . " WHERE user_id = ? ");
        $stmt->bind_param("si", $reg_id, $user_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        $stmt->close();

        if ($num_rows > 0) {
            $this->commitDB();
            return true;
        } else {
            // Failed to update user
            return false;
        }
    }

    //create Fostan
    public function createFostan($fostanSize, $fostanType, $fostanFor, $fostanPrice, $fostanTitle, $fostanDescription, $user_id, $show_number, $fostan_city, $fostanImg1, $fostanImg2, $fostanImg3) {

        $PhotoPath1 = $this->DecodeImage($fostanImg1);
        $PhotoPath2 = $this->DecodeImage($fostanImg2);
        $PhotoPath3 = $this->DecodeImage($fostanImg3);

        $stmt = $this->conn->prepare(
                "insert into fostans( fostan_title,fostan_proccess, fostan_price,fostan_size,fostan_desc,fostan_img1,fostan_img2,fostan_img3,fostan_city,show_number,user_id,fostan_type_id)"
                . " VALUES (?,?,?,?)");
        $stmt->bind_param("ssssssssiiii", $fostanTitle, $fostanFor, $fostanPrice, $fostanSize, $fostanDescription, $PhotoPath1, $PhotoPath2, $PhotoPath3, $fostan_city, $show_number, $user_id, $fostanType);

        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            return $this->conn->insert_id;
        } else {
            return null;
        }

        return null;
    }

    //Add Comment
    public function addComment($comment, $user_id, $fostan_id, $flag, $fostan_owner_user_email) {

        $stmt = $this->conn->prepare(
                "INSERT INTO comments (comment_text, user_id , fostan_id) VALUES(?,?,?)");
        $stmt->bind_param("sii", $comment, $user_id, $fostan_id);

        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            return $this->conn->insert_id;
        } else {
            return null;
        }

        return null;
    }

    //Set Favorite
    public function SetFavorite($user_id, $fostan_id) {

        $stmt = $this->conn->prepare(
                "INSERT INTO favorites (fav_user_id, fav_fostan_id) VALUES(?,?)");
        $stmt->bind_param("ii", $user_id, $fostan_id);

        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            return true;
        } else {
            return null;
        }
    }

    //Set Favorite
    public function SetAbuseReport($fostan_id) {

        $stmt = $this->conn->prepare(
                "INSERT INTO fostan_report (fostan_id) VALUES (?)");
        $stmt->bind_param("i", $fostan_id);

        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            return true;
        } else {
            return null;
        }
    }

    public function isFostanFavorite($fostan_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT fav_id from fostans ,favorites where fav_user_id =  ? and fav_fostan_id = ?");

        $stmt->bind_param("ii", $user_id, $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    //DELETE FOSTAN
    public function deleteFostan($FOSTAN_ID) {
        $response = array();
        $stmt = $this->conn->prepare("DELETE FROM fostans WHERE FOSTAN_ID = ? ");
        $stmt->bind_param("i", $FOSTAN_ID);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            // User successfully updated
            $response["error"] = false;
            $response['message_en'] = 'Deleted Successfully';
            $response['message_en'] = 'تم الحذف';

            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to delete";
            $response['message_ar'] = "لم يتم الحذف";
        }

        return $response;
    }

    //Get Fostan By Type ID
    public function getFostanByTypeID($fostan_type_id, $country_id, $startItem, $endItem) {

        $stmt = $this->conn->prepare("SELECT * from fostans where fostan_type_id = ? "
                . "and country_id = ? order by fostan_id desc LIMIT ?,?");

        $stmt->bind_param("iiii", $user_id, $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($fostan_id, $fostan_title, $fostan_proccess, $fostan_price, $fostan_size, $fostan_length, $fostan_sleeve_length, $fostan_desc, $fostan_img1, $fostan_img2, $fostan_img3, $show_number, $user_id, $fostan_type_id, $fostan_created_at, $country_id, $fostan_city, $fostan_viewer_count);
            $stmt->fetch();
            $FostanData = array();

            $FostanData["fostan_id"] = $fostan_id;
            $FostanData["fostan_title"] = $fostan_title;
            $FostanData["fostan_proccess"] = $fostan_proccess;
            $FostanData["fostan_price"] = $fostan_price;
            $FostanData["fostan_size"] = $fostan_size;
            $FostanData["fostan_length"] = $fostan_length;
            $FostanData["fostan_sleeve_length"] = $fostan_sleeve_length;
            $FostanData["fostan_desc"] = $fostan_desc;
            $FostanData["fostan_img1"] = $fostan_img1;
            $FostanData["fostan_img2"] = $fostan_img2;
            $FostanData["fostan_img3"] = $fostan_img3;
            $FostanData["show_number"] = $show_number;
            $FostanData["user_id"] = $user_id;
            $FostanData["fostan_type_id"] = $fostan_type_id;
            $FostanData["fostan_created_at"] = $fostan_created_at;
            $FostanData["country_id"] = $country_id;
            $FostanData["fostan_city"] = $fostan_city;
            $FostanData["fostan_viewer_count"] = $fostan_viewer_count;


            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $FostanData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get Fostan By ID
    public function getFostanByID($fostan_id, $user_id) {

        $this->updateViewersCount($fostan_id); //incremnt the viewers by ++1

        $stmt = $this->conn->prepare("SELECT fostan_title , fostan_proccess,fostan_price,fostan_size,fostan_desc,fostan_img1,fostan_img2,fostan_img3,fostan_city,show_number,"
                . "users.user_id,fostan_type_id,fostan_created_at,user_name,user_email,user_mobile,user_image,country_id"
                . " from fostans , users where fostan_id = ? and users.user_id = fostans.user_id");

        $stmt->bind_param("i", $fostan_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($fostan_id, $fostan_title, $fostan_proccess, $fostan_price, $fostan_size, $fostan_length, $fostan_sleeve_length, $fostan_desc, $fostan_img1, $fostan_img2, $fostan_img3, $show_number, $user_id, $fostan_type_id, $fostan_created_at, $country_id, $fostan_city, $fostan_viewer_count);
            $stmt->fetch();
            $FostanData = array();

            $FostanData["fostan_id"] = $fostan_id;
            $FostanData["fostan_title"] = $fostan_title;
            $FostanData["fostan_proccess"] = $fostan_proccess;
            $FostanData["fostan_price"] = $fostan_price;
            $FostanData["fostan_size"] = $fostan_size;
            $FostanData["fostan_length"] = $fostan_length;
            $FostanData["fostan_sleeve_length"] = $fostan_sleeve_length;
            $FostanData["fostan_desc"] = $fostan_desc;
            $FostanData["fostan_img1"] = $fostan_img1;
            $FostanData["fostan_img2"] = $fostan_img2;
            $FostanData["fostan_img3"] = $fostan_img3;
            $FostanData["show_number"] = $show_number;
            $FostanData["user_id"] = $user_id;
            $FostanData["fostan_type_id"] = $fostan_type_id;
            $FostanData["fostan_created_at"] = $fostan_created_at;
            $FostanData["country_id"] = $country_id;
            $FostanData["fostan_city"] = $fostan_city;
            $FostanData["fostan_viewer_count"] = $fostan_viewer_count;


            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $FostanData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get All Fostan Types
    public function getAllFostanTypes() {

        $stmt = $this->conn->prepare("SELECT * from fostans_types");

        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($fostan_type_id, $fostan_type_name, $fostan_type_img);
            $stmt->fetch();
            $FostanData = array();

            $FostanData["fostan_type_id"] = $fostan_type_id;
            $FostanData["fostan_type_name"] = $fostan_type_name;
            $FostanData["fostan_type_img"] = $fostan_type_img;

            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $FostanData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get Comments
    public function getComments($fostan_id, $startItem, $endItem) {

        $stmt = $this->conn->prepare("select comment_id , comment_text , comment_created_at , users.user_id , users.user_name from comments , users "
                . "where fostan_id = ? and users.user_id = comments.user_id order by comment_id desc LIMIT ?,?");

        $stmt->bind_param("iii", $fostan_id, $startItem, $endItem);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($comment_id, $comment_text, $fcomment_created_at, $user_id, $user_name);
            $stmt->fetch();
            $FostanData = array();

            $FostanData["comment_id"] = $comment_id;
            $FostanData["comment_text"] = $comment_text;
            $FostanData["comment_created_at"] = $fcomment_created_at;
            $FostanData["user_id"] = $user_id;
            $FostanData["user_name"] = $user_name;

            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $FostanData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get Countries
    public function getCountries() {

        $stmt = $this->conn->prepare("select * from countries");

        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($country_id, $country_name, $country_key);
            $stmt->fetch();
            $CountriesData = array();

            $CountriesData["country_id"] = $country_id;
            $CountriesData["country_name"] = $country_name;
            $CountriesData["country_key"] = $country_key;

            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $CountriesData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get Favorite
    public function getFavorite($user_id, $startItem, $endItem) {

        $stmt = $this->conn->prepare("SELECT * from fostans ,favorites where "
                . "fav_user_id = ? and fav_fostan_id = fostans.fostan_id order by fostan_id desc LIMIT ?,?");

        $stmt->bind_param("iii", $user_id, $startItem, $endItem);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($fostan_id, $fostan_title, $fostan_proccess, $fostan_price, $fostan_size, $fostan_length, $fostan_sleeve_length, $fostan_desc, $fostan_img1, $fostan_img2, $fostan_img3, $show_number, $user_id, $fostan_type_id, $fostan_created_at, $country_id, $fostan_city, $fostan_viewer_count);
            $stmt->fetch();
            $FostanData = array();

            $FostanData["fostan_id"] = $fostan_id;
            $FostanData["fostan_title"] = $fostan_title;
            $FostanData["fostan_proccess"] = $fostan_proccess;
            $FostanData["fostan_price"] = $fostan_price;
            $FostanData["fostan_size"] = $fostan_size;
            $FostanData["fostan_length"] = $fostan_length;
            $FostanData["fostan_sleeve_length"] = $fostan_sleeve_length;
            $FostanData["fostan_desc"] = $fostan_desc;
            $FostanData["fostan_img1"] = $fostan_img1;
            $FostanData["fostan_img2"] = $fostan_img2;
            $FostanData["fostan_img3"] = $fostan_img3;
            $FostanData["show_number"] = $show_number;
            $FostanData["user_id"] = $user_id;
            $FostanData["fostan_type_id"] = $fostan_type_id;
            $FostanData["fostan_created_at"] = $fostan_created_at;
            $FostanData["country_id"] = $country_id;
            $FostanData["fostan_city"] = $fostan_city;
            $FostanData["fostan_viewer_count"] = $fostan_viewer_count;

            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $FostanData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get Last Advertise From Users
    public function getLastAdvertiseFromUsers($country_id, $startItem, $endItem) {

        $stmt = $this->conn->prepare("select * from fostans where country_id = ? order by fostan_id desc LIMIT ?,?");

        $stmt->bind_param("iii", $country_id, $startItem, $endItem);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($fostan_id, $fostan_title, $fostan_proccess, $fostan_price, $fostan_size, $fostan_length, $fostan_sleeve_length, $fostan_desc, $fostan_img1, $fostan_img2, $fostan_img3, $show_number, $user_id, $fostan_type_id, $fostan_created_at, $country_id, $fostan_city, $fostan_viewer_count);
            $stmt->fetch();
            $FostanData = array();

            $FostanData["fostan_id"] = $fostan_id;
            $FostanData["fostan_title"] = $fostan_title;
            $FostanData["fostan_proccess"] = $fostan_proccess;
            $FostanData["fostan_price"] = $fostan_price;
            $FostanData["fostan_size"] = $fostan_size;
            $FostanData["fostan_length"] = $fostan_length;
            $FostanData["fostan_sleeve_length"] = $fostan_sleeve_length;
            $FostanData["fostan_desc"] = $fostan_desc;
            $FostanData["fostan_img1"] = $fostan_img1;
            $FostanData["fostan_img2"] = $fostan_img2;
            $FostanData["fostan_img3"] = $fostan_img3;
            $FostanData["show_number"] = $show_number;
            $FostanData["user_id"] = $user_id;
            $FostanData["fostan_type_id"] = $fostan_type_id;
            $FostanData["fostan_created_at"] = $fostan_created_at;
            $FostanData["country_id"] = $country_id;
            $FostanData["fostan_city"] = $fostan_city;
            $FostanData["fostan_viewer_count"] = $fostan_viewer_count;

            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $FostanData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get MyAdded Fostans
    public function getMyAddedFostans($user_id, $country_id, $startItem, $endItem) {

        $stmt = $this->conn->prepare("SELECT * from fostans where user_id = ? and country_id = ? order by fostan_id desc  LIMIT ?,? ");

        $stmt->bind_param("iiii", $user_id, $country_id, $startItem, $endItem);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($fostan_id, $fostan_title, $fostan_proccess, $fostan_price, $fostan_size, $fostan_length, $fostan_sleeve_length, $fostan_desc, $fostan_img1, $fostan_img2, $fostan_img3, $show_number, $user_id, $fostan_type_id, $fostan_created_at, $country_id, $fostan_city, $fostan_viewer_count);
            $stmt->fetch();
            $FostanData = array();

            $FostanData["fostan_id"] = $fostan_id;
            $FostanData["fostan_title"] = $fostan_title;
            $FostanData["fostan_proccess"] = $fostan_proccess;
            $FostanData["fostan_price"] = $fostan_price;
            $FostanData["fostan_size"] = $fostan_size;
            $FostanData["fostan_length"] = $fostan_length;
            $FostanData["fostan_sleeve_length"] = $fostan_sleeve_length;
            $FostanData["fostan_desc"] = $fostan_desc;
            $FostanData["fostan_img1"] = $fostan_img1;
            $FostanData["fostan_img2"] = $fostan_img2;
            $FostanData["fostan_img3"] = $fostan_img3;
            $FostanData["show_number"] = $show_number;
            $FostanData["user_id"] = $user_id;
            $FostanData["fostan_type_id"] = $fostan_type_id;
            $FostanData["fostan_created_at"] = $fostan_created_at;
            $FostanData["country_id"] = $country_id;
            $FostanData["fostan_city"] = $fostan_city;
            $FostanData["fostan_viewer_count"] = $fostan_viewer_count;

            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $FostanData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get Last Comment For User
    public function getLastCommentForUser($fostan_id, $user_id) {

        $stmt = $this->conn->prepare("select comment_id , comment_text , comment_created_at , users.user_id , users.user_name from comments , users "
                . "where fostan_id = ? and users.user_id = ? order by comment_id desc LIMIT 0,1");

        $stmt->bind_param("ii", $fostan_id, $user_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($comment_id, $comment_text, $user_id, $user_name, $comment_created_at);
            $stmt->fetch();
            $CommentData = array();

            $CommentData["comment_id"] = $comment_id;
            $CommentData["comment_text"] = $comment_text;
            $CommentData["user_id"] = $user_id;
            $CommentData["user_name"] = $user_name;
            $CommentData["comment_created_at"] = $comment_created_at;

            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $CommentData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get Conversation From User
    public function getConversationFromUser($user_from, $user_to, $startItem, $endItem) {

        $stmt = $this->conn->prepare("select  CASE WHEN msg_from = ? 
            then msg_to else msg_from 
                  end msg_from_result , msg_id,msg_from,msg_to,msg_content,msg_status,msg_created_at,
            (select user_name from users where user_id = msg_from_result) user_name
                   from messages 
            where ((msg_from = ? and msg_to = ?) or  (msg_from = ? and msg_to = ?) )
                order by msg_id desc limit ? , ?");

        $stmt->bind_param("iiiiiii", $user_from, $user_from, $user_to, $user_to, $user_from, $startItem, $endItem);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($msg_id, $msg_content, $msg_from, $msg_created_at, $fromUserName);
            $stmt->fetch();
            $ConversationData = array();

            $ConversationData["msg_id"] = $msg_id;
            $ConversationData["msg_content"] = $msg_content;
            $ConversationData["msg_from"] = $msg_from;
            $ConversationData["msg_created_at"] = $msg_created_at;
            $ConversationData["fromUserName"] = $fromUserName;

            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $ConversationData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Get Conversation From User
    public function getConversationForUser($user_id) {

        $stmt = $this->conn->prepare("select msg_id,msg_from,msg_to,msg_content,msg_status,msg_created_at,
                            (select user_name from users where user_id = case when msg_from = ? then msg_to else msg_from  end ) user_name 
                            from messages where msg_id in 
                            (select max(msg_id) from messages  
                            where (msg_to = ? or msg_from = ?)
                            group by case when msg_from = ? then msg_to else msg_from end ) order by msg_created_at desc");

        $stmt->bind_param("iiii", $user_id, $user_id, $user_id, $user_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($msg_id, $msg_content, $msg_to, $msg_from, $msg_status, $msg_created_at, $user_name);
            $stmt->fetch();
            $ConversationData = array();

            $ConversationData["msg_id"] = $msg_id;
            $ConversationData["msg_content"] = $msg_content;
            $ConversationData["msg_to"] = $msg_to;
            $ConversationData["msg_from"] = $msg_from;
            $ConversationData["msg_status"] = $msg_status;
            $ConversationData["msg_created_at"] = $msg_created_at;
            $ConversationData["user_name"] = $user_name;


            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $ConversationData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

    //Update Viewers Count
    public function updateViewersCount($fostan_id) {

        $response = array();
        if ($fostan_id != NULL) {
            $stmt = $this->conn->prepare("update fostans set fostan_viewer_count = fostan_viewer_count+1 where fostan_id = ?");
            $stmt->bind_param("i", $fostan_id);
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->affected_rows;
            if ($num_rows > 0) {
                // User successfully updated
                $response["error"] = false;
                $response['message_en'] = 'Viewers Count Updated';
            } else {
                // Failed to update user
                $response["error"] = true;
                $response['message_en'] = "Viewers Count Not Updated";
            }
            $stmt->close();
        } else {
            $response["error"] = true;
            $response['message_en'] = "An error occured";
        }
        return $response;
    }

    //DELETE FOSTAN
    public function deleteFavorite($fav_id) {
        $response = array();
        $stmt = $this->conn->prepare("delete from favorites where fav_id = ?");
        $stmt->bind_param("i", $fav_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->affected_rows;
        if ($num_rows > 0) {
            // User successfully updated
            $response["error"] = false;
            $response['message_en'] = 'Deleted Successfully';
            $response['message_en'] = 'تم الحذف';

            $this->commitDB();
        } else {
            // Failed to update user
            $response["error"] = true;
            $response['message_en'] = "Failed to delete";
            $response['message_ar'] = "لم يتم الحذف";
        }

        return $response;
    }

    //***********************************DB**************************************
    public function commitDB() {
        $this->conn->commit();
    }

    public function rollbackDB() {
        $this->conn->rollback();
    }

    //************************************SEARCH*************************************
    public function SEARCH($fostanSize, $fostanType, $fostanFor, $fostanPrice, $fostanCity, $startItem, $endItem, $country_idS) {

        if (!empty($fostanSize)) {
            $query = "select * from fostans where fostan_size  = '$fostanSize' ";

            if (!empty($fostanType)) {
                $query .= " and fostan_type_id = $fostanType ";
            }
            if (!empty($fostanFor)) {
                $query .= " and fostan_proccess = '$fostanFor' ";
            }
            if (!empty($fostanPrice)) {
                $query .= " and fostan_price <= $fostanPrice ";
            }
            if (!empty($fostanCity)) {
                $query .= " and fostan_city = $fostanCity ";
            }
        } else if (!empty($fostanType)) {
            $query = "select * from fostans where fostan_type_id = $fostanType  ";

            if (!empty($fostanSize)) {
                $query .= " and  fostan_size  = '$fostanSize' ";
            }
            if (!empty($fostanFor)) {
                $query .= " and fostan_proccess = '$fostanFor' ";
            }
            if (!empty($fostanPrice)) {
                $query .= " and fostan_price <= $fostanPrice ";
            }
            if (!empty($fostanCity)) {
                $query .= " and fostan_city = $fostanCity ";
            }
        } else if (!empty($fostanFor)) {
            $query = "select * from fostans where fostan_proccess = '$fostanFor'  ";

            if (!empty($fostanType)) {
                $query .= " and fostan_type_id = $fostanType ";
            }
            if (!empty($fostanSize)) {
                $query .= " and  fostan_size  = '$fostanSize' ";
            }
            if (!empty($fostanPrice)) {
                $query .= " and fostan_price <= $fostanPrice ";
            }
            if (!empty($fostanCity)) {
                $query .= " and fostan_city = $fostanCity ";
            }
        } else if (!empty($fostanPrice)) {
            $query = "select * from fostans where fostan_price <= $fostanPrice  ";

            if (!empty($fostanType)) {
                $query .= " and fostan_type_id = $fostanType ";
            }
            if (!empty($fostanFor)) {
                $query .= " and fostan_proccess = '$fostanFor' ";
            }
            if (!empty($fostanSize)) {
                $query .= " and  fostan_size  = '$fostanSize' ";
            }

            if (!empty($fostanCity)) {
                $query .= " and fostan_city = $fostanCity ";
            }
        } else if (!empty($fostanCity)) {
            $query = "select * from fostans where fostan_city = $fostanCity  ";

            if (!empty($fostanType)) {
                $query .= " and fostan_type_id = $fostanType ";
            }
            if (!empty($fostanFor)) {
                $query .= " and fostan_proccess = '$fostanFor' ";
            }
            if (!empty($fostanPrice)) {
                $query .= " and fostan_price <= $fostanPrice ";
            }
            if (!empty($fostanSize)) {
                $query .= " and  fostan_size  = '$fostanSize' ";
            }
        } else {
            $queryReady = true;
            $query = "select * from fostans where country_id = $country_id order by fostan_id desc limit $startItem,$endItem";
        }

        if (!$queryReady) {
            $query .= " and country_id = $country_id order by fostan_id desc limit $startItem,$endItem ";
        }



        $stmt = $query ;
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $response = array();
        if ($num_rows > 0) {

            $stmt->bind_result($fostan_id, $fostan_title, $fostan_proccess, $fostan_price, $fostan_size, $fostan_length, $fostan_sleeve_length, $fostan_desc, $fostan_img1, $fostan_img2, $fostan_img3, $show_number, $user_id, $fostan_type_id, $fostan_created_at, $country_id, $fostan_city, $fostan_viewer_count);
            $stmt->fetch();
            $FostanData = array();

            $FostanData["fostan_id"] = $fostan_id;
            $FostanData["fostan_title"] = $fostan_title;
            $FostanData["fostan_proccess"] = $fostan_proccess;
            $FostanData["fostan_price"] = $fostan_price;
            $FostanData["fostan_size"] = $fostan_size;
            $FostanData["fostan_length"] = $fostan_length;
            $FostanData["fostan_sleeve_length"] = $fostan_sleeve_length;
            $FostanData["fostan_desc"] = $fostan_desc;
            $FostanData["fostan_img1"] = $fostan_img1;
            $FostanData["fostan_img2"] = $fostan_img2;
            $FostanData["fostan_img3"] = $fostan_img3;
            $FostanData["show_number"] = $show_number;
            $FostanData["user_id"] = $user_id;
            $FostanData["fostan_type_id"] = $fostan_type_id;
            $FostanData["fostan_created_at"] = $fostan_created_at;
            $FostanData["country_id"] = $country_id;
            $FostanData["fostan_city"] = $fostan_city;
            $FostanData["fostan_viewer_count"] = $fostan_viewer_count;


            $response['error'] = false;
            $response["data"] = array();
            array_push($response["data"], $FostanData);

            $stmt->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message_en'] = "No Data Found";
            $response["message_ar"] = "لا يوجد بيانات";

            return $response;
        }
    }

}
