<?php
/**
 * Database configuration
 */
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'fostani');
 
define("GOOGLE_API_KEY", "AIzaSyCktFqeo2asLy2LcXqnIJUxWPzAUxH-VrE");
 
// push notification flags
define('PUSH_FLAG_CHATROOM', 100);
define('PUSH_FLAG_USER', 200);

define('PUSH_FLAG_CREATE_CHATROOM', 1);
define('PUSH_FLAG_CREATE_COURSES', 2);
define('PUSH_FLAG_CREATE_ASSIGNMENT', 3);
define('PUSH_FLAG_CREATE_QUIZ', 4);
define('PUSH_FLAG_CREATE_GRADE', 5);
define('PUSH_FLAG_CREATE_ANNOUNCEMENT', 6);

 