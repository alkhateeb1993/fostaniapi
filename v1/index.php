<?php

error_reporting(-1);
ini_set('display_errors', 'On');

require_once '../include/db_handler.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->response->headers->set('Content-Type', 'application/json');


/**
 * **************USERS TABLE ************
 * */
//User Login
$app->post('/user/login', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('user_name', 'user_password'));

    try {
        //reading post params
        $user_name = $app->request->post('user_name');
        $user_password = $app->request->post('user_password');

        $db = new DbHandler();
        $response = $db->login($user_name, $user_password);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//create user
$app->post('/user/create_user', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('USER_NAME', 'USER_PASSWORD', 'USER_EMAIL', 'USER_MOBILE', 'USER_COUNTRY_ID', 'USER_GENDER', 'USER_REG_ID', 'USER_PIC'));

    //reading post params
    try {
        $USER_NAME = $app->request->post('USER_NAME');
        $USER_PASSWORD = $app->request->post('USER_PASSWORD');
        $USER_EMAIL = $app->request->post('USER_EMAIL');
        $USER_MOBILE = $app->request->post('USER_MOBILE');
        $USER_COUNTRY_ID = $app->request->post('USER_COUNTRY_ID');
        $USER_GENDER = $app->request->post('USER_GENDER');
        $USER_REG_ID = $app->request->post('USER_REG_ID');
        $USER_PIC = $app->request->post('USER_PIC');

        $db = new DbHandler();
        $response = $db->createUser($USER_NAME, $USER_PASSWORD, $USER_EMAIL, $USER_MOBILE, $USER_COUNTRY_ID, $USER_GENDER, $USER_REG_ID, $USER_PIC);
        $db->commitDB();
    } catch (Exception $ex) {
        $db->rollbackDB();

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }



    echoResponse(200, $response);
});

//Update REG ID
$app->post('/user/update_regid', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('gcm_registration_id', 'user_id'));

    try {
        $gcm_registration_id = $app->request->post('gcm_registration_id');
        $user_id = $app->request->post('user_id');
        $db = new DbHandler();
        if ($db->updateRegID($user_id, $gcm_registration_id)) {
            $response["error"] = false;
            $response["message"] = "Reg ID Updated Successfully";
        } else {
            $response["error"] = true;
            $response["message"] = "Reg ID Not Updated ";
        }
    } catch (Exception $ex) {
        $db->rollbackDB();

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }

    echoResponse(200, $response);
});



//create Fostan
$app->post('/user/create_fostan', function() use ($app) {


    verifyRequiredParams(array(
        'fostanSize',
        'fostanType',
        'fostanFor',
        'fostanPrice',
        'fostan_title',
        'fostanDescription',
        'user_id',
        'show_number',
        'fostanCity'));

    $fostanSize = $app->request->post('fostanSize');
    $fostanType = $app->request->post('fostanType');
    $fostanFor = $app->request->post('fostanFor');
    $fostanPrice = $app->request->post('fostanPrice');
    $fostanTitle = $app->request->post('fostan_title');
    $fostanDescription = $app->request->post('fostanDescription');
    $user_id = $app->request->post('user_id');
    $show_number = $app->request->post('show_number');
    $fostan_city = $app->request->post('fostanCity');
    $fostanImg1 = $app->request->post('fostanImg1');
    $fostanImg2 = $app->request->post('fostanImg2');
    $fostanImg3 = $app->request->post('fostanImg3');

    $db = new DbHandler();
    $response = array();
    try {
        $fostan_id = $db->createFostan($fostanSize, $fostanType, $fostanFor, $fostanPrice, $fostanTitle, $fostanDescription, $user_id, $show_number, $fostan_city, $fostanImg1, $fostanImg2, $fostanImg3);

        if ($fostan_id != null) {

//            if (!empty($_FILES['uploaded_file1']['name'])) {
//                $file_name1 = basename($_FILES['uploaded_file1']['name']);
//                $file_content_1 = $_FILES['uploaded_file1']['tmp_name'];
//
//                $ATTACHMENT_ID1 = $db->uploadAttachments($file_name1, $file_content_1);
//                if (!($ATTACHMENT_ID1 != null && $db->insertAttachmentForFostan($fostan_id, $ATTACHMENT_ID1) != null)) {
//                    $response["error"] = true;
//                    $response["message"] = "Attachment 1 not uploaded successfully";
//                    $db->rollbackDB();
//                    echoResponse(400, $response);
//                    $app->stop();
//                }
//            }
//
//            if (!empty($_FILES['uploaded_file2']['name'])) {
//                $file_name2 = basename($_FILES['uploaded_file2']['name']);
//                $file_content_2 = $_FILES['uploaded_file2']['tmp_name'];
//
//                $ATTACHMENT_ID2 = $db->uploadAttachments($file_name2, $file_content_2);
//                if (!($ATTACHMENT_ID2 != null && $db->insertAttachmentForFostan($fostan_id, $ATTACHMENT_ID2) != null)) {
//                    $response["error"] = true;
//                    $response["message"] = "Attachment 2 not uploaded successfully";
//                    $db->rollbackDB();
//                    echoResponse(400, $response);
//                    $app->stop();
//                }
//            }
//
//            if (!empty($_FILES['uploaded_file3']['name'])) {
//                $file_name3 = basename($_FILES['uploaded_file3']['name']);
//                $file_content_3 = $_FILES['uploaded_file3']['tmp_name'];
//
//                $ATTACHMENT_ID3 = $db->uploadAttachments($file_name3, $file_content_3);
//                if (!($ATTACHMENT_ID3 != null && $db->insertAttachmentForFostan($fostan_id, $ATTACHMENT_ID3) != null)) {
//                    $response["error"] = true;
//                    $response["message"] = "Attachment 3 not uploaded successfully";
//                    $db->rollbackDB();
//                    echoResponse(400, $response);
//                    $app->stop();
//                }
//            }

            $response["error"] = false;
            $response["message_en"] = "Fostan Has Been Created Successfully";
            $response["message_ar"] = "تم اضافة الفستان بنجاح";

            // $db->sendNewCreationNotification(PUSH_FLAG_CREATE_FOSTANS, $DEPT_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $fostan_id);
            $db->commitDB();
        } else {
            $response["error"] = true;
            $response["message_en"] = "Fostan Not Created";
            $response["message_ar"] = "لم يتم اضافة الفستان";
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message_en"] = "Error Occurred";
        $response["message_ar"] = "حصلت مشكله";
        $response["description"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//create Comment
$app->post('/user/create_comment', function() use ($app) {


    verifyRequiredParams(array(
        'comment',
        'user_id',
        'fostan_id',
        'flag',
        'fostan_owner_user_email'));

    $comment = $app->request->post('comment');
    $user_id = $app->request->post('user_id');
    $fostan_id = $app->request->post('fostan_id');
    $flag = $app->request->post('flag');
    $fostan_owner_user_email = $app->request->post('fostan_owner_user_email');

    $db = new DbHandler();
    $response = array();
    try {
        $comment_id = $db->addComment($comment, $user_id, $fostan_id, $flag, $fostan_owner_user_email);

        if ($comment_id != null) {

            $response["error"] = false;
            $response["message_en"] = "Created Successfully";
            $response["message_ar"] = "تمت إضافة التعليق بنجاح";

            // $db->sendNewCreationNotification(PUSH_FLAG_CREATE_FOSTANS, $DEPT_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $fostan_id);
            $db->commitDB();
        } else {
            $response["error"] = true;
            $response["message_en"] = "Failed To Add Comment";
            $response["message_ar"] = "لم يتم اضافة التعليق";
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message_en"] = "Error Occurred";
        $response["message_ar"] = "حصلت مشكله";
        $response["description"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Set Favorite
$app->post('/user/setFavorite', function() use ($app) {


    verifyRequiredParams(array(
        'user_id',
        'fostan_id',
    ));

    $user_id = $app->request->post('user_id');
    $fostan_id = $app->request->post('fostan_id');

    $db = new DbHandler();
    $response = array();
    try {
        $result = $db->SetFavorite($user_id, $fostan_id);

        if ($result != null) {

            $response["error"] = false;
            $response["message_en"] = "Created Successfully";
            $response["message_ar"] = "تم التعديل";

            // $db->sendNewCreationNotification(PUSH_FLAG_CREATE_FOSTANS, $DEPT_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $fostan_id);
            $db->commitDB();
        } else {
            $response["error"] = true;
            $response["message_en"] = "Failed To Add";
            $response["message_ar"] = "لم يتم التعديل";
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message_en"] = "Error Occurred";
        $response["message_ar"] = "حصلت مشكله";
        $response["description"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});


//Set Abuse Report
$app->post('/user/setAbuseReport', function() use ($app) {


    verifyRequiredParams(array(
        'fostan_id',
    ));

    $fostan_id = $app->request->post('fostan_id');

    $db = new DbHandler();
    $response = array();
    try {
        $result = $db->SetAbuseReport($fostan_id);

        if ($result != null) {

            $response["error"] = false;
            $response["message_en"] = "Created Successfully";
            $response["message_ar"] = "تم التعديل";

            // $db->sendNewCreationNotification(PUSH_FLAG_CREATE_FOSTANS, $DEPT_ID, $FOSTAN_NAME, $INSTRUCTOR_ID, $fostan_id);
            $db->commitDB();
        } else {
            $response["error"] = true;
            $response["message_en"] = "Failed To Add";
            $response["message_ar"] = "لم يتم التعديل";
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message_en"] = "Error Occurred";
        $response["message_ar"] = "حصلت مشكله";
        $response["description"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});


// is fostan favorite
$app->post('/user/isFostanFavorite', function() use ($app) {

    verifyRequiredParams(array(
        'fostan_id',
        'user_id'));

    $fostan_id = $app->request()->post('fostan_id');
    $user_id = $app->request()->post('user_id');

    $db = new DbHandler();
    $response = array();
    try {
        if ($db->isFostanFavorite($fostan_id, $user_id)) {
            $response["error"] = false;
            $response["message_en"] = "Fostan is Favorite";
        } else {
            $response["error"] = true;
            $response["message_en"] = "Not Favorite";
        }
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message_en"] = "Error Occurred";
        $response["message_ar"] = "حصلت مشكله";
        $response["description"] = $ex->getMessage();
    }
});

//Delete Fostan
$app->post('/user/delete_fostan', function() use ($app) {
    verifyRequiredParams(array('fostan_id'));

    try {
        $FOSTAN_ID = $app->request->post('fostan_id');

        $db = new DbHandler();
        $response = $db->deleteFostan($FOSTAN_ID);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message_en"] = "Error Occurred";
        $response["message_ar"] = "حصلت مشكله";
        $response["description"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Delete Favorite
$app->post('/user/delete_favorite', function() use ($app) {
    verifyRequiredParams(array('fav_id'));

    try {
        $fav_id = $app->request->post('fav_id');

        $db = new DbHandler();
        $response = $db->deleteFavorite($fav_id);
    } catch (Exception $ex) {
        $response["error"] = true;
        $response["message_en"] = "Error Occurred";
        $response["message_ar"] = "حصلت مشكله";
        $response["description"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Fostans By Type ID
$app->post('/user/getFostanByTypeID', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('fostan_type_id', 'country_id', 'startItem', 'endItem'));

    try {
        //reading post params
        $fostan_type_id = $app->request->post('fostan_id');
        $country_id = $app->request->post('country_id');
        $startItem = $app->request->post('startItem');
        $endItem = $app->request->post('endItem');

        $db = new DbHandler();
        $response = $db->getFostanByTypeID($fostan_type_id, $country_id, $startItem, $endItem);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Fostans By ID
$app->post('/user/getFostanByTypeID', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('fostan_id', 'user_id'));

    try {
        //reading post params
        $fostan_id = $app->request->post('fostan_id');
        $user_id = $app->request->post('user_id');


        $db = new DbHandler();
        $response = $db->getFostanByID($fostan_id, $user_id);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get All Fostan Types
$app->post('/user/getAllFostanTypes', function() use ($app) {
    //checking require params
    try {
        $db = new DbHandler();
        $response = $db->getAllFostanTypes();
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});


//Get Comments
$app->post('/user/getComments', function() use ($app) {
    //checking require params
    verifyRequiredParams(array('fostan_id', 'startItem', 'endItem'));

    try {
        //reading post params
        $fostan_id = $app->request->post('fostan_id');
        $startItem = $app->request->post('startItem');
        $endItem = $app->request->post('endItem');

        $db = new DbHandler();
        $response = $db->getComments($fostan_id, $startItem, $endItem);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Countries
$app->post('/user/getCountries', function() use ($app) {

    try {

        $db = new DbHandler();
        $response = $db->getCountries();
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Favorite
$app->post('/user/getFavorite', function() use ($app) {

    verifyRequiredParams(array('fostan_id', 'user_id'));

    try {
        //reading post params
        $user_id = $app->request->post('user_id');
        $startItem = $app->request->post('startItem');
        $endItem = $app->request->post('endItem');

        $db = new DbHandler();
        $response = $db->getFavorite($user_id, $startItem, $endItem);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});




//Get Last Advertise From Users
$app->post('/user/LastAdvertiseFromUsers', function() use ($app) {

    verifyRequiredParams(array('country_id', 'startItem', 'endItem'));

    try {
        //reading post params
        $country_id = $app->request->post('country_id');
        $startItem = $app->request->post('startItem');
        $endItem = $app->request->post('endItem');

        $db = new DbHandler();
        $response = $db->getFavorite($country_id, $startItem, $endItem);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get My Added Fostans
$app->post('/user/getMyAddedFostans', function() use ($app) {

    verifyRequiredParams(array('user_id', 'country_id', 'startItem', 'endItem'));

    try {
        //reading post params
        $user_id = $app->request->post('user_id');
        $country_id = $app->request->post('country_id');
        $startItem = $app->request->post('startItem');
        $endItem = $app->request->post('endItem');

        $db = new DbHandler();
        $response = $db->getMyAddedFostans($user_id, $country_id, $startItem, $endItem);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Last Comment For User
$app->post('/user/getLastCommentForUser', function() use ($app) {

    verifyRequiredParams(array('fostan_id', 'user_id'));

    try {
        //reading post params
        $fostan_id = $app->request->post('fostan_id');
        $user_id = $app->request->post('user_id');

        $db = new DbHandler();
        $response = $db->getLastCommentForUser($fostan_id, $user_id);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Conversation From User
$app->post('/user/getConversationFromUser', function() use ($app) {

    verifyRequiredParams(array('user_from', 'user_to', 'startItem', 'endItem'));

    try {
        //reading post params
        $user_from = $app->request->post('user_from');
        $user_to = $app->request->post('user_to');
        $startItem = $app->request->post('startItem');
        $endItem = $app->request->post('endItem');

        $db = new DbHandler();
        $response = $db->getConversationFromUser($user_from, $user_to, $startItem, $endItem);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

//Get Conversation For User
$app->post('/user/getConversationForUser', function() use ($app) {

    verifyRequiredParams(array('user_id'));

    try {
        //reading post params
        $user_from = $app->request->post('user_from');
        $db = new DbHandler();
        $response = $db->getConversationForUser($user_id);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});


//SEARCH
$app->post('/user/getFavorite', function() use ($app) {

    verifyRequiredParams(array('fostanSize', 'fostanType', 'fostanFor', 'fostanPrice', 'fostanCity', 'startItem', 'endItem', 'country_id'));

    try {
        //reading post params
        $fostanSize = $app->request->post('fostanSize');
        $fostanType = $app->request->post('fostanType');
        $fostanFor = $app->request->post('fostanFor');
        $fostanPrice = $app->request->post('fostanPrice');
        $fostanCity = $app->request->post('fostanCity');
        $startItem = $app->request->post('startItem');
        $endItem = $app->request->post('endItem');
        $country_id = $app->request->post('country_id');


        $db = new DbHandler();
        $response = $db->SEARCH($fostanSize, $fostanType, $fostanFor, $fostanPrice, $fostanCity, $startItem, $endItem, $country_id);
    } catch (Exception $ex) {

        $response["error"] = true;
        $response["message"] = $ex->getMessage();
    }
    echoResponse(200, $response);
});

/** * *************************** Helper Functions
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
    // setting response content type to json
    $app->contentType('application/json');
    echo json_encode($response);
}

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoResponse(400, $response);
        $app->stop();
    }
}

function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

$app->run();

